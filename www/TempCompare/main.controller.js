(function () {
  'use strict';

  angular
    .module('kwtrg.main')
    .controller('MainController', MainController);

  MainController.$inject = ['$rootScope', '$scope', '$state', 'ionicMaterialMotion', 'mainService', '$ionicPopup', '$cordovaInAppBrowser', '$filter',
    '$ionicLoading', 'homeDataService', 'AddBo3qarService', '$ionicModal', '$ionicSlideBoxDelegate', '$interval'
  ];

  /* @ngInject */
  function MainController($rootScope, $scope, $state, ionicMaterialMotion, mainService, $ionicPopup, $cordovaInAppBrowser, $filter,
    $ionicLoading, homeDataService, AddBo3qarService, $ionicModal, $ionicSlideBoxDelegate, $interval) {

    var vm = angular.extend(this, {
      nextAccountsWalkthrough: nextAccountsWalkthrough,
      nextAccountsWalkthrough2: nextAccountsWalkthrough2,
      showAccountsWalkthrough: false,
      showBo3qarWalkthrough: false,
      navigateHome: navigateHome,
      slider: [],
      valid_elements: [],
      Count: 0,
      skip_count: 0
    });

    var skipAd = 0;
    var userInfo = -1;
    var IsRegister = 0;
    userInfo = JSON.parse(window.localStorage.getItem('user'));

    function navigateHome() {


      if (window.localStorage.getItem("user") === null) {

        //This mean anonymous user a=with acitve request

        $ionicLoading.show({
          duration: 60000,
          noBackdrop: true,
          template: ' <div class="spinner2">  <ion-spinner  icon="ripple" class="spinner-assertive"></ion-spinner>  </div> </ion-content>'
        });

        AddBo3qarService.HaveActiveRequest(function (data) {

          if (data === "1") {

            $state.go('app.tabs.bo3qars2'); //This mean anonymous user a=with acitve request
          } else {

            $state.go('app.tabs.AddBo3qar2'); //This is anonymous user new request
          }
          $ionicLoading.hide({});

        });


      } else {

        $state.go('app.tabs.requests'); //This is a company 
      }


    }

    // ********************************************************************

    //TOUR
    var mainTour = window.localStorage.getItem('mainTour');

    if (mainTour == 1 || mainTour === null || !angular.isDefined(mainTour)) {
      setTimeout(function () {
        vm.showAccountsWalkthrough = true;
        window.localStorage.setItem('mainTour', 0);

      }, 500);
    } else if (mainTour == 0) {

      setTimeout(function () {
        vm.showBo3qarWalkthrough = true;
        window.localStorage.setItem('mainTour', 2);

      }, 500);
    }


    $rootScope.accountsWalkthrough = {
      steps: [{

          text: "img/homeb.png"
        },

        {

          text: "img/homea.png"
        }
      ],
      currentIndex: 0
    };

    if (window.localStorage.getItem("user") === null) {
      $rootScope.accountsWalkthrough2 = {
        steps: [{

            text: "img/notification.png"
          },

          {

            text: "img/Request.png"
          }
        ],
        currentIndex: 0
      };
    } else {
      $rootScope.accountsWalkthrough2 = {
        steps: [{

            text: "img/notification.png"
          },

          {

            text: "img/RequestReg.png"
          }
        ],
        currentIndex: 0
      };
    }

    $scope.$on('$ionicView.beforeEnter', function (event, viewData) {

      $ionicModal.fromTemplateUrl('modal.html', function ($ionicModal) {
        $scope.modal = $ionicModal;
      }, {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
      });

      $scope.openModal = function () {

        $scope.modal.show();

      };

      $scope.close_add = function () {

        if (skipAd === 1)
          $scope.modal.hide();
      }

      $scope.open_link = function (link) {
        //console.log(link);
        if (link !== null)
          window.open(link, '_system', 'location=yes');
      }

      //GetItemsBo3qarCount
      if (window.localStorage.getItem("user") === null) {

        AddBo3qarService.GetItemsBo3qarCount(function (data) {

          vm.Count = data;
          // alert(vm.Count);

        });
      } else {

        AddBo3qarService.GetRequestsBo3qarCount(function (data) {

          vm.Count = data;
          // alert(vm.Count);

        });
      }

    });

    function nextAccountsWalkthrough() {


      ionic.requestAnimationFrame(function () {

        if ($scope.accountsWalkthrough.currentIndex < $scope.accountsWalkthrough.steps.length - 1) {
          $scope.accountsWalkthrough.currentIndex++;
          vm.showAccountsWalkthrough = true;
        }
      });

    }

    function nextAccountsWalkthrough2() {


      ionic.requestAnimationFrame(function () {

        if ($scope.accountsWalkthrough2.currentIndex < $scope.accountsWalkthrough2.steps.length - 1) {
          $scope.accountsWalkthrough2.currentIndex++;
          vm.showBo3qarWalkthrough = true;
        }
      });

    }

    //ENDOFTOUR
    // Set Motion
    ionicMaterialMotion.fadeSlideInRight();

    (function activate() {

      //push notificatio


      //end of push     

      ///DATA VERSION CHECK
      console.log("-----CHECKING DATA VERSIONS------");
      mainService.getDataVersion(function (data) {
        var Version = data["VALUE"];
        var DataVersion = window.localStorage.getItem('DataVersion');
        console.log("-----Version-->" + Version);
        console.log("-----DataVersion-->" + DataVersion);
        if (DataVersion === null || !angular.isDefined(DataVersion) || DataVersion !== Version) {
          console.log("DataVersion-->" + DataVersion);
          console.log("DataUpdate-->" + Version);
          $ionicLoading.show({
            duration: 30000,
            noBackdrop: true,
            template: ' <div class="spinner2">  <ion-spinner  icon="ripple" class="spinner-assertive"></ion-spinner>  </div> </ion-content>'
          });

          homeDataService.Types(function (data) {
            window.localStorage.setItem("Gallary", JSON.stringify(data));
          });

          setTimeout(function () {
            window.localStorage.setItem('DataVersion', Version);

          }, 500);
        }

      });

      // POPUP
      setTimeout(function () {



        // POPUP AD
        mainService.getPopupAd(function (data) {

          //console.log(window.localStorage.getItem('Country_ID'));
          //console.log(data);

          //console.log("/********************AD*************************/");        

          var skip_text = 'Waiting...';
          var skip_counter = 1;
          var skip_time = 0;
          if (data != null && data.status === "OK") {

            skip_time = data.result[0].skip_time;

            vm.popupad = "data:image/png;base64," + data.result[0].image;
            vm.skip_count = skip_text;
            vm.adlink = data.result[0].link;

            $ionicLoading.show({
              duration: 3000,
              noBackdrop: true,
              template: ' <div class="spinner2">  <ion-spinner  icon="ripple" class="spinner-assertive"></ion-spinner>  </div> </ion-content>'
            });

            //window.localStorage.getItem('showAd') === null &&
            if (data.result[0].valid_ad === true) {
              $scope.modal.show();
              //window.localStorage.setItem("showAd", 1);
            }

            var timer = $interval(function () {

              var skip_diff = skip_time - skip_counter;
              vm.skip_count = skip_diff;
              if (skip_counter === skip_time) {

                vm.can_skip = true;
                vm.skip_count = "Skip Ad";
                skipAd = 1;
                $interval.cancel(timer);
              }

              //console.log(skip_counter);
              skip_counter++;

            }, 1000);


            //console.log("/********************AD*************************/");
          }

        });

        if ($ionicSlideBoxDelegate._instances[0] !== undefined) {
          vm.slider = [];
          vm.valid_elements = [];
          vm.statusad = false;
          $ionicSlideBoxDelegate._instances[0].kill();
          $ionicSlideBoxDelegate.update();
        } 

        // SLIDER
        mainService.getSliderAd(function (data) {

          if (data.status === "OK") {

            vm.slider = data.result;

            var counter = 0;
            var next_slide = function () {

              var scroll_time = vm.valid_elements[counter].slider_scroll === 0 ? 1000 : vm.valid_elements[counter].slider_scroll;

              $ionicSlideBoxDelegate.$getByHandle('IOBOX').next(1000);
              $ionicSlideBoxDelegate.update();
              $ionicSlideBoxDelegate.loop(true);

              //console.log("HERE");
              counter++
              if (counter === vm.valid_elements.length)
                counter = 0;

              setTimeout(next_slide, scroll_time * 1000);
            };

            $ionicLoading.show({
              duration: 3000,
              noBackdrop: true,
              template: ' <div class="spinner2">  <ion-spinner  icon="ripple" class="spinner-assertive"></ion-spinner>  </div> </ion-content>'
            });

            data.result.forEach(element => {

              //console.log(element);
              if (element.valid_ad === true)
                vm.valid_elements.push(element);
            });

            next_slide();
            vm.statusad = true;
            console.log("/********************SLIDER*************************/");

          } else {
            vm.statusad = false;
          }

        });

      }, 500);

    })();


    $scope.$on('$.afterLeave', function (event, viewData) {


    });


    $scope.$on('$ionicView.beforeLeave', function (event, viewData) {


      if ($ionicSlideBoxDelegate._instances[0] !== undefined) {
        vm.slider = [];
        vm.valid_elements = [];
        vm.statusad = false;
        $ionicSlideBoxDelegate._instances[0].kill();
        //$ionicSlideBoxDelegate.update();
      } 
      
      
      mainService.getSliderAd(function (data) {

        
        if (data.status === "OK") {

         

          console.log(data.result);
          
          vm.slider = data.result;

          var counter = 0;
          var next_slide = function () {

            var scroll_time = vm.valid_elements[counter].slider_scroll === 0 ? 1000 : vm.valid_elements[counter].slider_scroll;

            $ionicSlideBoxDelegate.$getByHandle('IOBOX').next(1000);
            $ionicSlideBoxDelegate.update();
            $ionicSlideBoxDelegate.loop(true);

            //console.log("HERE");
            counter++
            if (counter === vm.valid_elements.length)
              counter = 0;

            setTimeout(next_slide, scroll_time * 1000);
          };

          $ionicLoading.show({
            duration: 3000,
            noBackdrop: true,
            template: ' <div class="spinner2">  <ion-spinner  icon="ripple" class="spinner-assertive"></ion-spinner>  </div> </ion-content>'
          });

          data.result.forEach(element => {

            //console.log(element);
            if (element.valid_ad === true)
              vm.valid_elements.push(element);
          });

          next_slide();
          vm.statusad = true;
          console.log("/********************SLIDER*************************/");

        } else {
          vm.statusad = false;
        }

      });

      // Return 1 if a > b
      // Return -1 if a < b
      // Return 0 if a == b
      function compare(a, b) {
        if (a === b) {
          return 0;
        }

        var a_components = a.split(".");
        var b_components = b.split(".");

        var len = Math.min(a_components.length, b_components.length);

        // loop while the components are equal
        for (var i = 0; i < len; i++) {
          // A bigger than B
          if (parseInt(a_components[i]) > parseInt(b_components[i])) {
            return 1;
          }

          // B bigger than A
          if (parseInt(a_components[i]) < parseInt(b_components[i])) {
            return -1;
          }
        }

        // If one's a prefix of the other, the longer one is greater.
        if (a_components.length > b_components.length) {
          return 1;
        }

        if (a_components.length < b_components.length) {
          return -1;
        }

        // Otherwise they are the same.
        return 0;
      }

      ///////APPLICATION VERSION CHECK
      mainService.getVersion(function (data) {

        console.log("Calling Verion-->");
        if (data === null) {
          alert("Error : System version: 101");
        } else {

          var Version = data["VALUE"];
          var Update = data["Note"];
          console.log("Version-->" + Version);
          console.log("appVersion-->" + $rootScope.appVersion);
          console.log(compare(Version, $rootScope.appVersion));
          if (compare(Version, $rootScope.appVersion) == 1) {
            switch (Update) {
              case undefined:

                break;

              case "1":
                var alertPopup = $ionicPopup.alert({
                  title: "يجب تحديث التطبيق",
                  template: "{{ 'Fupdate' | translate }}"
                });
                var x = $filter('translate')('Fupdate');
                //  alert(ionic.Platform.platform());
                alert(x);

                var scheme;

                // Don't forget to add the org.apache.cordova.device plugin!
                if (ionic.Platform.platform() === 'ios') {
                  scheme = 'twitter://';
                  window.open('http://appstore.com', '_system', 'location=yes');
                } else if (ionic.Platform.platform() === 'android') {
                  scheme = 'com.twitter.android';
                  window.open('https://play.google.com/', '_system', 'location=yes');

                }

                $rootScope.myGoBack();
                ionic.Platform.exitApp();
                break;

              case "2":

                var alertPopup = $ionicPopup.alert({
                  title: "يجب تحديث التطبيق",
                  template: "{{ 'Fupdate' | translate }}"
                });

                break;

              case "3":
                //Version
                console.log("--Checking Data version--");
                var DataVersion = window.localStorage.getItem('DataVersion');
                var allcountry = JSON.parse(window.localStorage.getItem('allCountry'));

                if (DataVersion === null || !angular.isDefined(DataVersion) || DataVersion !== Version || allcountry === null || !angular.isDefined(allcountry)) {
                  console.log("DataVersion-->" + DataVersion);
                  console.log("DataUpdate-->" + Version);
                  $ionicLoading.show({
                    duration: 30000,
                    noBackdrop: true,
                    template: ' <div class="spinner2">  <ion-spinner  icon="ripple" class="spinner-assertive"></ion-spinner>  </div> </ion-content>'
                  });

                  homeDataService.Types(function (data) {
                    window.localStorage.setItem("Gallary", JSON.stringify(data));
                  });
                  homeDataService.allCountry(function (data) {
                    window.localStorage.setItem("allCountry", JSON.stringify(data));
                  });
                  homeDataService.allCity(function (data) {
                    window.localStorage.setItem("allCity", JSON.stringify(data));
                  });
                  homeDataService.allRegion(function (data) {
                    $ionicLoading.hide({});
                    window.localStorage.setItem("allRegion", JSON.stringify(data));
                  });

                  setTimeout(function () {

                    window.localStorage.setItem('DataVersion', Version);

                  }, 500);
                }



                break;


            }
          }
        }

      });

    });


  }
})();
