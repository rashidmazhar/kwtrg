(function() {
	'use strict';
	
	var propertiesView = {
		templateUrl: 'js/main/main.html',
		controller: 'MainController as vm'
	};


	angular
		.module('kwtrg.main', [
			'ionic'
		])
		.config(function($stateProvider) {
			$stateProvider

		 
			.state('app.tabs.main', {
					url: '/main',
								views: { 'tab-home': propertiesView }
					}
				)
			;
			
		});
})();
