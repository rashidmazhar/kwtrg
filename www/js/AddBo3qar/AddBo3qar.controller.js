(function() {
	'use strict';

	angular
		.module('kwtrg.AddBo3qar')
		.controller('AddBo3qarController', AddBo3qarController);

	 
    	AddBo3qarController.$inject = [ '$ionicActionSheet','$timeout','$rootScope','$scope','$state','$ionicPopup','filterFilter','$ionicLoading','$stateParams','AddBo3qarService','$filter','$cordovaCamera'];

	/* @ngInject */
	function AddBo3qarController($ionicActionSheet,$timeout,$rootScope,$scope,$state,$ionicPopup,filterFilter, $ionicLoading,$stateParams,AddBo3qarService,$filter,$cordovaCamera, $cordovaFileTransfer,$cordovaImagePicker) {
	var picData =""; 
  
   var allcountry = JSON.parse( window.localStorage.getItem('allCountry'));
		var allCity = JSON.parse( window.localStorage.getItem('allCity'));
		var allRegions = JSON.parse( window.localStorage.getItem('allRegion'));
		var allTypes = JSON.parse( window.localStorage.getItem('Gallary')); 
		var Lang=window.localStorage.getItem('lang');
	  var CountryID =parseInt(window.localStorage.getItem('Country_ID'));
      

		var vm = angular.extend(this, {
			entries: [],
            City: [],
            Region: [],
            Types: [],
            Feature: [],
            DayCalculator :'',
            validAd: false
          	 
		});
	


var numberofImages =0;
var userInfo=-1;
   var  id =0;
var remoteFiles = [];
  (function activate() {
    
	  vm.entries =allcountry;
      $scope.selectedcountry= CountryID;
      vm.City =	filterFilter(allCity, {Country_ID:CountryID},true);
      vm.Types=  filterFilter(allTypes, {Country_ID:CountryID},true);
      userInfo = JSON.parse( window.localStorage.getItem('user'));
           
			
		})();

    
var uri = window.localStorage.getItem('domain')+'/api/KwtrgDB/SaveImage';
  console.log("kordy URI= " + uri); 
 
 
  $scope.open_link = function (link) {      
    if(link !== null)
      window.open(link, '_system', 'location=yes');
  }      

  setTimeout(function () {

    // AD
    AddBo3qarService.GetBrokerAd(function (data) {



      $ionicLoading.show({
        duration: 3000,
        noBackdrop: true,
        template: ' <div class="spinner2">  <ion-spinner  icon="ripple" class="spinner-assertive"></ion-spinner>  </div> </ion-content>'
      });


      //console.log("/********************BROKER*************************/");
      console.log(data);

      if (data.status === "OK") {
        vm.brokerAd = "data:image/png;base64," + data.result[0].image;
        vm.brokerLink = data.result[0].link;
        vm.validAd = data.result[0].valid_ad;
      }
      //console.log(vm.validAd);       
    });


  }, 50); 

 




$scope.$on('$ionicView.loaded', function (event, viewData) {	
    viewData.enableBack = true;

    
   
});


/*ENDOFCAMERA*/
   
     

			$scope.$on('$ionicView.beforeEnter', function (event, viewData) {
  viewData.enableBack = true;
    
  if (window.localStorage.getItem("user") !==null)
      {
        $state.go('app.tabs.AddItem');
      }
 
   
}); 
      
    /*objects*/
              
         
    /*EndOfobjects*/
             
 
           $scope.change = function(selectedItem, SelectService) {
           

         vm.City =	filterFilter(allCity, {Country_ID:parseInt(selectedItem)},true);
		     vm.Types=  filterFilter(allTypes, {Country_ID:parseInt(selectedItem), Service_ID:parseInt(SelectService) },true);   

       };

        $scope.changeService= function(selectedItem, SelectService) {

         vm.City =	filterFilter(allCity, {Country_ID:parseInt(selectedItem)},true);
		     vm.Types=  filterFilter(allTypes, {Country_ID:parseInt(selectedItem), Service_ID:parseInt(SelectService) },true);   
                     
        

       };
       

             



            $scope.changeCity = function(selectedCity) {
            	vm.Region =	filterFilter(allRegions, {City_ID:parseInt(selectedCity)},true); 
             };       
           
              
	      

           $scope.changeType = function(selectedType) {
               
               
              AddBo3qarService.getFeature(function(data){
		           	vm.Feature = data;             
                
                },selectedType,CountryID);  
             };  



             $scope.changeFeature = function(selectedFeature) {
                var array = selectedFeature.split(';');
                var Ucs_Id = parseInt(array[0].trim());
                var Ucs_Value = parseInt(array[1].trim()) ;         
                $filter('filter')(vm.Feature, function (d) {return d.Ucs_Id === Ucs_Id;})[0]["SelectedValue"]=Ucs_Value;      
            
              
             };  

              
        /*Submit*/
         $scope.submit = function(selectedService,selectedcountry,selectedType,selectedCity,selectedRegion) {
            // if (	$('#I_0').hasClass('clz_blur') || $('#I_1').hasClass('clz_blur') ||  $('#I_2').hasClass('clz_blur')|| $('#I_3').hasClass('clz_blur'))
            //   {
            //                     var alertPopup = $ionicPopup.alert({
            //                             title: 'الرجاء الانتظار',
            //                             template: ' جاري تحميل صور الاعلان الخاص بك الرجاء الانتظار و المحاولة بعد قليل في حال استمرار المشكله قم بحذف الصوره و اعادة تحميلها  للمساعدة تواصل مع الدعم الفني'
            //                             });
            //   }
            //  else{
                                    $ionicLoading.show({
                                    duration:60000,	 
                                    noBackdrop: true,
                                    template: ' <div class="spinner2">  <ion-spinner  icon="ripple" class="spinner-assertive"></ion-spinner>  </div> </ion-content>' });
                                    // alert(id);
                                    var Item = 
                                            {
                                            "Service_Id":selectedService,
                                                "Country_ID" :selectedcountry,
                                                "Type_ID": selectedType ,
                                                "City_ID":selectedCity,
                                                "Region_ID":selectedRegion,
                                                "Title":"",
                                                "Body":"",
                                                "PhoneNo":0,
                                                "Price":0,
                                                "Area":0,
                                                "CreatedBy":"",
                                                "UCS":vm.Feature,
                                                "Session":1,
                                                "Token":""

                                            };
                                       
                                        AddBo3qarService.InserItem (function(data){
                                             
                                                        if (data==="1")
                                                        {
                                                            var alertPopup = $ionicPopup.alert({
                                                                title: 'تم اضافة طلبك بنجاح ',
                                                                template: "{{ 'ٍRequestDone' | translate }}"
                                                                });
                                                            
                                                            $state.go('app.tabs.main');
                                                        }
                                                        else if (data==="10")
                                                        {
                                                            var alertPopup = $ionicPopup.alert({
                                                            title: 'لا يمكنك المتابعة ',
                                                            template: "{{ 'ExceedBo3qar' | translate }}"
                                                            });
                                                        
                                                        }
                                                 $ionicLoading.hide({	});
                                            
                                            },Item)   ;
                        //   }
            
             }
        /*END*/     

       
	}
})();