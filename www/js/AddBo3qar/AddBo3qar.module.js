(function() {
	'use strict';
	
	var propertiesView = {
		templateUrl: 'js/AddBo3qar/AddBo3qar.html',
		controller: 'AddBo3qarController as vm'
	};

 


	angular
		.module('kwtrg.AddBo3qar', [
			'ionic'
			, 'ngCordova'
			 
		])
		.config(function($stateProvider) {
			$stateProvider

			.state('app.tabs.AddBo3qar', {
				cache: false,
					url: '/AddBo3qar/',
								views: { 'tab-Ads': propertiesView }
					}

					
				)


				.state('app.tabs.AddBo3qar2', {
					cache: false,
					   url: '/AddBo3qar',
								   views: { 'tab-Ads': propertiesView }
					   }
   
					   
				   )
			


	      
			;
			
		});

		
})();
