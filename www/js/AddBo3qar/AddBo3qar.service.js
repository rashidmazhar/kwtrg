(function() {
	'use strict';

	angular
		.module('kwtrg.AddBo3qar')
		.factory('AddBo3qarService', AddBo3qarService);

	AddBo3qarService.$inject = ['$http', '$q'];
	
	 

	/* @ngInject */
	function AddBo3qarService($http, $q) {
		var url = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetCountriesApp?Lang='+window.localStorage.getItem('lang');
		var result = [];
		var resultCity = [];
		var resultRegion = [];
		var resultType = [];
		var FeatureType = [];
		var DayCalculatorType = [];

		var service = {
			all: all,
            allCity: allCity,
            allRegion: allRegion,
			allTypes: allTypes,
			getFeature: getFeature,
			InserItem:InserItem,
			HaveActiveRequest:HaveActiveRequest,
			GetItemsBo3qarCount:GetItemsBo3qarCount,
			GetRequestsBo3qarCount:GetRequestsBo3qarCount,
			GetBrokerAd: GetBrokerAd
		};
		return service;

		// *******************************************************
 
		function all(callback){
		 
			$http.get(url)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					result = data;
				    
					callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					//alert('Error : Try Again');
					console.log('ERROR (AddBo3qar-all):' +data+ status);
					callback(result);
				});
			
			  
			 
		}


        function allCity(callback, id){
		 var urlCity = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetCitiesByCountriesApp?Lang='+window.localStorage.getItem('lang')+"&id="+id;
			$http.get(urlCity)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					resultCity = data;
				    
					callback(resultCity);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					//alert('Error : Try Again');
					console.log('ERROR (additem-allcity):'+data + status);
					callback(resultCity);
				});
	 
		}

         function allRegion(callback, id){
		 var urlCity = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetRegionsByCitiesApp?Lang='+window.localStorage.getItem('lang')+"&id="+id;
			$http.get(urlCity)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					resultRegion = data;
				    
					callback(resultRegion);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					//alert('Error : Try Again');
					console.log('ERROR (additem-allRegion):'+ data+ status);
					callback(resultRegion);
				});
			
			  
			 
		}    

              function allTypes(callback, id){
		 var urlType = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetTypeByCountryIDApp?Lang='+window.localStorage.getItem('lang')+"&id="+id;
			$http.get(urlType)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					resultType = data;
				    
					callback(resultType);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					//alert('Error : Try Again');
					console.log('ERROR (additem+allTypes):'+data + status);
					callback(resultType);
				});
			
			  
			 
		} 
			

		function getFeature(callback ,TypeID,CountryID) {
		 
		 var urlFeature = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetTypeFieldsApp?Lang='+window.localStorage.getItem('lang')+"&Type="+TypeID+"&CountryId="+CountryID;
		 
		$http.get(urlFeature)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					FeatureType = data;
				  
					callback(FeatureType);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					//alert('Error : Try Again');
					console.log('ERROR (additem-getFeature):'+data + status);
					callback(FeatureType);
				});
	}

        

	
	function InserItem(callback, Item) {
		 
		var token  =  window.localStorage.getItem('registrationId');

			if (token === undefined || token ==="" || token===null || token==="null")
			{
				token="fx7_ym7VDsY:APA91bF5z1kK0cVgcmzCk1Uf2b_HPQKAeDmN4-hgx2qNz2CWoyY5UiOmvZWQCoA6QTBBJeORpB1AyyixDCMYys70_xhpwQ0d51GDkgtmuMqKMF5oRKoNMtWnLADamSWpfhw8H6mcbD_i";
			}
			Item.Token = token;
		//	alert(Item.Service_Id +"-"+ Item.Country_ID +"-" +Item.Type_ID+"-"+Item.City_ID+"-"+Item.Region_ID+"-"+Item.UCS+"-"+Item.Token);
		 var urlInsert = window.localStorage.getItem('domain')+'/api/KwtrgDB/InsetToBo3qarApp';
		 
           $http({
                url: urlInsert,
                method: 'POST',
				dataType: "json",
                data:  Item,
				contentType: "application/json",
                //headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
			.success(function(data, status, headers, config,serverData) {
					// this callback will be called asynchronously
					// when the response is available
					//alert( data);
				 // alert("Data:-->"+ data);
				  console.log("ServerData:", serverData);
				  //alert(data);
				  callback(data);
					 
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					//alert('Error : Try Again'+status+ data);
					console.log('ERROR (additem-InserItem):'+data + status);
					 
				});
                 


	}


		
	function HaveActiveRequest(callback, id){
		var token  =  window.localStorage.getItem('registrationId');

		if (token === undefined || token ==="" || token===null || token==="null")
		{
			token="fx7_ym7VDsY:APA91bF5z1kK0cVgcmzCk1Uf2b_HPQKAeDmN4-hgx2qNz2CWoyY5UiOmvZWQCoA6QTBBJeORpB1AyyixDCMYys70_xhpwQ0d51GDkgtmuMqKMF5oRKoNMtWnLADamSWpfhw8H6mcbD_i";
		}
	
		 var url = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetHaveActiveRequest?_token='+token;
	 
		$http.get(url)
			.success(function(data, status, headers, config) {
				// this callback will be called asynchronously
				// when the response is available
				result = data;
				
				callback(result);
			})
			.error(function(data, status, headers, config) {
				// called asynchronously if an error occurs
				// or server returns response with an error status.
				//alert('Error : Try Again');
				console.log('ERROR (additem-allcity):'+data + status);
				callback(result);
			});
 
	}//END OF EDIT 


	function GetItemsBo3qarCount(callback ) {

		var token  =  window.localStorage.getItem('registrationId');

		if (token === undefined || token ==="" || token===null || token==="null")
		{
			token="fx7_ym7VDsY:APA91bF5z1kK0cVgcmzCk1Uf2b_HPQKAeDmN4-hgx2qNz2CWoyY5UiOmvZWQCoA6QTBBJeORpB1AyyixDCMYys70_xhpwQ0d51GDkgtmuMqKMF5oRKoNMtWnLADamSWpfhw8H6mcbD_i";
		}
	
		 
		 var url = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetItemsBo3qarCount?_token='+token;
		 
		$http.get(url)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					data;
				  
					callback(data);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					//alert('Error : Try Again');
					console.log('ERROR (additem-getFeature):'+data + status);
					callback(data);
				});
	}//end of DayAutoDeleteCalculator
	
	function GetRequestsBo3qarCount(callback ){
		var token  =  window.localStorage.getItem('registrationId');

			if (token === undefined || token ==="" || token===null || token==="null")
			{
				token="fx7_ym7VDsY:APA91bF5z1kK0cVgcmzCk1Uf2b_HPQKAeDmN4-hgx2qNz2CWoyY5UiOmvZWQCoA6QTBBJeORpB1AyyixDCMYys70_xhpwQ0d51GDkgtmuMqKMF5oRKoNMtWnLADamSWpfhw8H6mcbD_i";
			}
		  
		 var url = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetItemsRequestCount?_token='+token; 
		 
			$http.get(url)
				.success(function(data, status, headers, config) {
					
					// this callback will be called asynchronously
					// when the response is available
					result = data;
					//alert(data);
					callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					alert('no');
					console.log('ERROR (Count):' + status);
					callback(result);
				});
		}

		function GetBrokerAd(callback) {
  
			var urlData = window.localStorage.getItem('domain') + '/api/KwtrgDB/GetAd/2/' + window.localStorage.getItem('Country_ID');
			$http.get(urlData)
			  .success(function (data, status, headers, config) {
				// this callback will be called asynchronously
				// when the response is available
				result = data;
				//console.log(data);
				callback(result);
			  })
			  .error(function (data, status, headers, config) {
				// called asynchronously if an error occurs
				// or server returns response with an error status.
				//alert('no');
				console.log('ERROR (main):' + data + status);
				callback(result);
			  });
		  }



	}
})();