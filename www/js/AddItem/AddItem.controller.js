(function() {
	'use strict';

	angular
		.module('kwtrg.AddItem')
		.controller('AddItemController', AddItemController);

	 
    	AddItemController.$inject = [ '$ionicActionSheet','$timeout','$rootScope','$scope','$state','homeDataService','$ionicPopup','filterFilter','$ionicLoading','$stateParams','AddItemService','$filter','$cordovaCamera'];

	/* @ngInject */
	function AddItemController($ionicActionSheet,$timeout,$rootScope,$scope,$state,homeDataService,$ionicPopup,filterFilter, $ionicLoading,$stateParams,AddItemService,$filter,$cordovaCamera, $cordovaFileTransfer,$cordovaImagePicker) {
	var picData =""; 
  
       var allcountry = JSON.parse( window.localStorage.getItem('allCountry'));
		var allCity = JSON.parse( window.localStorage.getItem('allCity'));
		var allRegions = JSON.parse( window.localStorage.getItem('allRegion'));
        var allTypes = JSON.parse( window.localStorage.getItem('Gallary')); 
        
        if (   allcountry ===null ||  allCity ===null ||  allRegions ===null  ||  allTypes ===null 
            || allcountry === undefined || allCity === undefined || allRegions === undefined || allTypes === undefined 
            || allcountry ===""  || allCity ==="" || allRegions ==="" || allTypes ===""  )
        {
            $ionicLoading.show({			
                duration:30000,	 
                noBackdrop: true,
                template: ' <div class="spinner2">  <ion-spinner  icon="ripple" class="spinner-assertive"></ion-spinner>  </div> </ion-content>'
                });
            homeDataService.Types(function(data){ window.localStorage.setItem("Gallary",  JSON.stringify(data)); });
            homeDataService.allCountry(function(data){ window.localStorage.setItem("allCountry",  JSON.stringify(data)); });
            homeDataService.allCity(function(data){ window.localStorage.setItem("allCity",  JSON.stringify(data)); });
            homeDataService.allRegion(function(data){  $ionicLoading.hide({	}); window.localStorage.setItem("allRegion",  JSON.stringify(data)); });

          allcountry = JSON.parse( window.localStorage.getItem('allCountry'));
		  allCity = JSON.parse( window.localStorage.getItem('allCity'));
		  allRegions = JSON.parse( window.localStorage.getItem('allRegion'));
          allTypes = JSON.parse( window.localStorage.getItem('Gallary'));
        }

          

		var Lang=window.localStorage.getItem('lang');
	  var CountryID =parseInt(window.localStorage.getItem('Country_ID'));
       	$('#I_'+ 0).removeClass("clz_blur"); 
      	$('#I_'+ 1).removeClass("clz_blur"); 
        	$('#I_'+ 2).removeClass("clz_blur"); 
          	$('#I_'+ 3).removeClass("clz_blur"); 

		var vm = angular.extend(this, {
			entries: [],
            City: [],
            Region: [],
            Types: [],
            Feature: [],
            DayCalculator :''
          	 
		});
	


var numberofImages =0;
var userInfo=-1;
   var  id =0;
var remoteFiles = [];
  (function activate() {
   
     $scope.FirstImage = $filter('translate')('FistImage');
      $scope.SecondImage = $filter('translate')('SecondImage');
       $scope.ThirdImage = $filter('translate')('ThirdImage');
        $scope.FourthImage = $filter('translate')('FourthImage');
      $scope.ShowPathy=[{id: 0, value:false}, {id: 1, value:false}, {id: 2, value:false}, {id: 3, value:false}]; 
      $scope.ImagePathy =[{id: 0, value:"Img/slider2.png"}, {id: 1, value:"Img/slider2.png"}, {id: 2, value:"Img/slider2.png"}, {id: 3, value:"Img/slider2.png"}];
                  //id = Math.floor(Math.random()*9999);
                   id = parseInt ( Math.floor(Math.random()*9999))+   parseInt ($filter('date')(new Date(), 'dd')) + parseInt ( $filter('date')(new Date(), 'MM')) + parseInt (  $filter('date')(new Date(), 'HH') ) +  ($filter('date')(new Date(), 'mm')) +   ( $filter('date')(new Date(), 'ss')) ;
			 
		  vm.entries =allcountry;
      $scope.selectedcountry= CountryID;
      vm.City =	filterFilter(allCity, {Country_ID:CountryID},true);
      vm.Types=  filterFilter(allTypes, {Country_ID:CountryID},true);
      userInfo = JSON.parse( window.localStorage.getItem('user'));
        
      //alert (userInfo["IsActive"]);
      
      if (window.localStorage.getItem("user") ===null)
      {
         
        //  var alertPopup = $ionicPopup.alert({
        //         title: 'لا يمكنك المتابعة ',
        //         template: "{{ 'loginRequired' | translate }}"  
        //          });
               
        // $state.go('login');
        $state.go('app.tabs.AddBo3qar');
       
         
      }
      else if (userInfo["IsActive"] ===0)
      {

        var alertPopup = $ionicPopup.alert({
                title: 'لا يمكنك المتابعة ',
                template: "{{ 'credentialsAct' | translate }}"  
                 });
               
        $state.go('app.tabs.main');
      }
      
			
		})();






     $scope.show = function(duration) {
                  
                   
                              console.log('working action');
                            // Show the action sheet
                            var hideSheet = $ionicActionSheet.show({
                                buttons: [
                                { text: '<i class="icon-left ion-camera"></i>    ' + $filter('translate')('Camera')},
                                { text: '<i class=" icon-left ion-images "></i>   '+$filter('translate')('Import')} 
                                ],
                                destructiveText:  $filter('translate')('Delete'),
                                destructiveButtonClicked: function() {
                                      
                                     $scope.ShowPathy[0].value=false;
                                     $scope.ImagePathy[0].value= "Img/slider2.png";
                                    return true; //Close the model?
                                },
                                titleText: $filter('translate')('FistImageSelect'),  
                                cancelText: 'Cancel',
                                cancel: function() {
                                       
                                    // add cancel code..
                                    },
                                buttonClicked: function(index) {
                                     if(index === 0){ // Manual Button   
                                    
                         
                           
                                           $scope.takePhoto(index);
                                        }
                                        else if(index === 1){
                                       
                                            $scope.selImages(index-1);
                                        }
                                return true;
                                }
                            });

                            // For example's sake, hide the sheet after two seconds
                            $timeout(function() {
                                hideSheet();
                            }, duration);

                            };//END OF SHOW


//ACTIONSHEET SecondImage

 $scope.showSc = function(duration) {
    
                              console.log('working action re');
                            // Show the action sheet
                            var hideSheet = $ionicActionSheet.show({
                                buttons: [
                                { text: '<i class="icon-left ion-camera"></i>    ' + $filter('translate')('Camera')},
                                { text: '<i class=" icon-left ion-images "></i>   '+$filter('translate')('Import')} 
                                ],
                                destructiveText:  $filter('translate')('Delete'),
                                destructiveButtonClicked: function() {
                                    $scope.ShowPathy[1].value=false;
                                     $scope.ImagePathy[1].value= "Img/slider2.png";
                                    return true; //Close the model?
                                },
                                titleText: $filter('translate')('SecondImageSelect'),  
                                cancelText: 'Cancel',
                                cancel: function() {
                                       
                                    // add cancel code..
                                    },
                                buttonClicked: function(index) {
                                     if(index === 0){ // Manual Button   
                                     
                                       
                                            $scope.takePhoto(1);
                                        }
                                        else if(index === 1){
                                       
                                            $scope.selImages(1);
                                        }
                                return true;
                                }
                            });

                            // For example's sake, hide the sheet after two seconds
                            $timeout(function() {
                                hideSheet();
                            }, duration);

                            };//END OF SHOW



//ACTIONSHEET ThirdImage

 $scope.showTh = function(duration) {
      
                              console.log('working action re');
                            // Show the action sheet
                            var hideSheet = $ionicActionSheet.show({
                                buttons: [
                                { text: '<i class="icon-left ion-camera"></i>    ' + $filter('translate')('Camera')},
                                { text: '<i class=" icon-left ion-images "></i>   '+$filter('translate')('Import')} 
                                ],
                                destructiveText:  $filter('translate')('Delete'),
                                destructiveButtonClicked: function() {
                                    $scope.ShowPathy[2].value=false;
                                     $scope.ImagePathy[2].value= "Img/slider2.png";
                                    return true; //Close the model?
                                },
                                titleText: $filter('translate')('ThirdImageSelect'),  
                                cancelText: 'Cancel',
                                cancel: function() {
                                       
                                    // add cancel code..
                                    },
                                buttonClicked: function(index) {
                                     if(index === 0){ // Manual Button   
                                     
                        
                                            $scope.takePhoto(2);
                                        }
                                        else if(index === 1){
                                       
                                            $scope.selImages(2);
                                        }
                                return true;
                                }
                            });

                            // For example's sake, hide the sheet after two seconds
                            $timeout(function() {
                                hideSheet();
                            }, duration);

                            };//END OF SHOW



//ACTIONSHEET FourthImage

 $scope.showFu = function(duration) {
     
                              console.log('working action re');
                            // Show the action sheet
                            var hideSheet = $ionicActionSheet.show({
                                buttons: [
                                { text: '<i class="icon-left ion-camera"></i>    ' + $filter('translate')('Camera')},
                                { text: '<i class=" icon-left ion-images "></i>   '+$filter('translate')('Import')} 
                                ],
                                destructiveText:  $filter('translate')('Delete'),
                                destructiveButtonClicked: function() {
                                    $scope.ShowPathy[3].value=false;
                                     $scope.ImagePathy[3].value= "Img/slider2.png";
                                    return true; //Close the model?
                                },
                                titleText: $filter('translate')('FourthImageSelect'),  
                                cancelText: 'Cancel',
                                cancel: function() {
                                       
                                    // add cancel code..
                                    },
                                buttonClicked: function(index) {
                                     if(index === 0){ // Manual Button   
                                     
                        
                                            $scope.takePhoto(3);
                                        }
                                        else if(index === 1){
                                       
                                            $scope.selImages(3);
                                        }
                                return true;
                                }
                            });

                            // For example's sake, hide the sheet after two seconds
                            $timeout(function() {
                                hideSheet();
                            }, duration);

                            };//END OF SHOW







   
/*CAMERA*/

$scope.takePhoto = function (newIndex) {
    
                  var options = {
                    quality: 75,
                    destinationType: Camera.DestinationType.DATA_URL,
                    sourceType: Camera.PictureSourceType.CAMERA,
                    allowEdit: true,
                    encodingType: Camera.EncodingType.JPEG,
                   
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: true
                };
                 $('#I_'+ newIndex).addClass("clz_blur"); 
   
                    $cordovaCamera.getPicture(options).then(function (imageData) {
                        var imgURI = "data:image/jpeg;base64," + imageData;
                        
                         
                          //var newIndex = numberofImages;
                          console.log("img id= " + id);  
                         // console.log('Image URI: ' + imgURI);
                        
                             $scope.ShowPathy[newIndex].value=true; 
                    $scope.ImagePathy[newIndex].value= imgURI;
                    $scope.show(0);
                        // $('#container_main').prepend(
                        //       '<div class="container"><img class="clz_blur" src="'+imgURI+' "  id="I_'+ newIndex + '" width="100%" />' +
                        //       '<svg  class="load-svg" viewbox="0 0 250 250" >' +
                        //       '<path class="border" id="b_'+ newIndex + '" transform="translate(125, 125)"/>' +
                        //       '<path class="loader" id="l_'+ newIndex + '" transform="translate(125, 125) scale(.84)" /></svg></div>'); 

                        console.log("pathy... "+   $scope.ImagePathy);
                      console.log("Uploding... " );  
                      remoteFiles[newIndex]=imgURI;
                       DoUploadCamera (newIndex);  
                        numberofImages=numberofImages+1; 

                    }, function (err) {
                      alert(err);
                        // An error occured. Show a message to the user
                    });
                }
                
  //  $scope.choosePhoto = function () {
  //    alert ("Choosing ");
  //                 var options = {
  //                   quality: 75,
  //                   destinationType: Camera.DestinationType.DATA_URL,
  //                   sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
  //                   allowEdit: true,
  //                   encodingType: Camera.EncodingType.JPEG,
  //                   targetWidth: 300,
  //                   targetHeight: 300,
  //                   popoverOptions: CameraPopoverOptions,
  //                   saveToPhotoAlbum: false
  //               };
   
  //                   $cordovaCamera.getPicture(options).then(function (imageData) {
  //                       $scope.imgURI = "data:image/jpeg;base64," + imageData;
  //                   }, function (err) {
  //                     alert(err);
  //                       // An error occured. Show a message to the user
  //                   });
  //               }



   function hasReadPermission() {
                    window.imagePicker.hasReadPermission(
                    function(result) {
                        // if this is 'false' you probably want to call 'requestReadPermission' now
                        alert(result);
                    }
                    )
                }

                function requestReadPermission() {
                    // no callbacks required as this opens a popup which returns async
                    window.imagePicker.requestReadPermission();
                }



$scope.selImages = function(id2) {
     
   window.imagePicker.getPictures(
	function(results) {
     
     remoteFiles=results;
    //	  id = Math.floor(Math.random()*9999); //Random ID for image tag. You can use your own array of ID   
        console.log("KORDY THE ID IS NEW= " + id2); 
        var leno =results.length+id2;
        if (leno>4)
        {
          leno = 4-id2;
        }
        else
        {
          leno =results.length;
        }
      
    
		for (var i = 0; i < leno; i++) {
        
      var newIndex = (i+id2);
       $('#I_'+ newIndex).addClass("clz_blur"); 
      console.log("img id= " + id);  
			console.log('Image URI: ' + results[i]);
      console.log('newIndex--> '+newIndex);
       console.log('results.length--> '+results.length);
       
         $scope.ShowPathy[newIndex].value=true; 
                             $scope.ImagePathy[newIndex].value= results[i];
                             $scope. show(0);
	
		

        
		        
		}

       DoUpload(results.length-1,newIndex );
	}, function (error) {
		console.log('Error: ' + error);
	}, {
		maximumImagesCount: 4,
   
	 quality:75
	}
);
    };


    
       

function DoUpload (Length, Index){
 
    var newIndex = (Index);
  console.log("Uploading Call For -->"+Index+"-->"+ remoteFiles[Index] + "   ID--> "+id+" New Index --> " + newIndex);
    var FileName="Unknown";
    switch (Index) {
            case 0:
               FileName= id+"_FirstPhoto.jpeg";
               console.log("Current File-->"+FileName + "  Index-->"+Index );
                
                break;
            case 1:
                  FileName= id+"_SecondPhoto.jpeg";
               console.log("Current File-->"+FileName + "  Index-->"+Index );
                break;

           case 2:
                  FileName= id+"_ThirdPhoto.jpeg";
               console.log("Current File-->"+FileName + "  Index-->"+Index );
                break;

           case 3:
                  FileName= id+"_FourthPhoto.jpeg";
               console.log("Current File-->"+FileName + "  Index-->"+Index );
                break;
            default:
             FileName= "_undefined.jpeg";
               console.log("Current File-->"+FileName + "  Index-->"+Index );
               break;

        }
  
    var uri = window.localStorage.getItem('domain')+'/api/KwtrgDB/SaveImage';
          
        var options = new FileUploadOptions();
        options.fileKey="Filedata";
        options.fileName=remoteFiles[Length].substr(remoteFiles[Length].lastIndexOf('/') + 1);
          options.chunkedMode= false;
        options.mimeType= "image/*";
          var params = new Object(); 
                    params.session1 = FileName ; 
                
                    options.params = params; 
        var headers={'headerParam':'headerValue'};

        options.headers = headers;

        var ft = new FileTransfer();
        ft.onprogress = function(progress) {
           console.log("Onprog-->"+newIndex+"-->"+ remoteFiles[Length]);
            var percent  = Math.round(progress.loaded / progress.total *360);
                  var loader = document.getElementById('b_'+ newIndex), 
                      border = document.getElementById('l_'+ newIndex),
                      a = 0, 
                    p = Math.PI; 
                  
                  (function draw() {
                    a = percent; 
                    a %= 360;
                    var r = a * p / 180, x = Math.sin(r) * 125, y = Math.cos(r) * -125, mid = a > 180 ? 1 : 0, 
                        anim = 'M 0 0 v -125 A 125 125 1 ' + mid + ' 1 ' + x + ' ' + y + ' z';
                  
                    loader.setAttribute('d', anim); 
                    border.setAttribute('d', anim);
                  
                  if (a !== 0){
                    draw();
                  } else if (a === 0){
                    $('#I_'+ newIndex).removeClass("clz_blur"); 
                  }
                    
                  }()); 
        };
        ft.upload(remoteFiles[Length], uri,function(r) {
                // Do what you want with successful file downloaded and then 
                // call the method again to get the next file
              console.log("Current Item"+Index+"Code = " + r.responseCode);
              console.log("Current Item"+Index+"DResponse = " + r.response);
              console.log("Current Item"+Index+"Sent = " + r.bytesSent);
                if (Length!=0)
                {
                  DoUpload(Length-1,newIndex-1);
                }
                 
            }, function(r) {
             alert("Current Item"+Index+"An error has occurred: Code = " + error.code);
            console.log("Current Item"+Index+"upload error source " + error.source);
            console.log("Current Item"+Index+"upload error target " + error.target);
            }, options);
                     
      

           


}




function DoUploadCamera (Index){
 
    var newIndex = (Index);
  console.log("Uploading Call For -->"+Index+"-->"+ "removed" + "   ID--> "+id+" New Index --> " + newIndex);
    var FileName="Unknown";
    switch (Index) {
            case 0:
               FileName= id+"_FirstPhoto.jpeg";
               console.log("Current File-->"+FileName + "  Index-->"+Index );
                
                break;
            case 1:
                  FileName= id+"_SecondPhoto.jpeg";
               console.log("Current File-->"+FileName + "  Index-->"+Index );
                break;

           case 2:
                  FileName= id+"_ThirdPhoto.jpeg";
               console.log("Current File-->"+FileName + "  Index-->"+Index );
                break;

           case 3:
                  FileName= id+"_FourthPhoto.jpeg";
               console.log("Current File-->"+FileName + "  Index-->"+Index );
                break;
            default:
             FileName= "_undefined.jpeg";
               console.log("Current File-->"+FileName + "  Index-->"+Index );
               break;

        }
  
    var uri = window.localStorage.getItem('domain')+'/api/KwtrgDB/SaveImage';
          
        var options = new FileUploadOptions();
        options.fileKey="Filedata";
        options.fileName=remoteFiles[Index].substr(remoteFiles[Index].lastIndexOf('/') + 1);
          options.chunkedMode= false;
        options.mimeType= "image/*";
          var params = new Object(); 
                    params.session1 = FileName ; 
                
                    options.params = params; 
        var headers={'headerParam':'headerValue'};

        options.headers = headers;

        var ft = new FileTransfer();
        ft.onprogress = function(progress) {
           console.log("Onprog-->"+newIndex+"-->");
            var percent  = Math.round(progress.loaded / progress.total *360);
                  var loader = document.getElementById('b_'+ newIndex), 
                      border = document.getElementById('l_'+ newIndex),
                      a = 0, 
                    p = Math.PI; 
                  
                  (function draw() {
                    a = percent; 
                    a %= 360;
                    var r = a * p / 180, x = Math.sin(r) * 125, y = Math.cos(r) * -125, mid = a > 180 ? 1 : 0, 
                        anim = 'M 0 0 v -125 A 125 125 1 ' + mid + ' 1 ' + x + ' ' + y + ' z';
                  
                    loader.setAttribute('d', anim); 
                    border.setAttribute('d', anim);
                  
                  if (a !== 0){
                    draw();
                  } else if (a === 0){
                    $('#I_'+ newIndex).removeClass("clz_blur"); 
                  }
                    
                  }()); 
        };
        ft.upload(remoteFiles[Index], uri,function(r) {
                // Do what you want with successful file downloaded and then 
                // call the method again to get the next file
              console.log("Current Item"+Index+"Code = " + r.responseCode);
              console.log("Current Item"+Index+"DResponse = " + r.response);
              console.log("Current Item"+Index+"Sent = " + r.bytesSent);
               
                 
            }, function(r) {
             alert("Current Item"+Index+"An error has occurred: Code = " + error.code);
            console.log("Current Item"+Index+"upload error source " + error.source);
            console.log("Current Item"+Index+"upload error target " + error.target);
            }, options);
                     
}


 


$scope.uploadfile = function(){
     var options = {
        quality: 75,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM,
        mediaType: Camera.MediaType.ALLMEDIA
        //saveToPhotoAlbum: true
    };

    $cordovaCamera.getPicture(options).then(function(imageData) {
        console.log("img URI= " + imageData);        
        
	//	var  id = Math.floor(Math.random()*9999); //Random ID for image tag. You can use your own array of ID   
     console.log("img id= " + id);   
		$('#container_main').prepend(
          '<div class="container"><img class="clz_blur" src="'+imageData+' "  id="I_'+ id + '" width="100%" />' +
          '<svg  class="load-svg" viewbox="0 0 250 250" >' +
          '<path class="border" id="b_'+ id + '" transform="translate(125, 125)"/>' +
          '<path class="loader" id="l_'+ id + '" transform="translate(125, 125) scale(.84)" /></svg></div>'); 
		
		   
		var server = "http://128.199.75.202/uploads/upload.php",
        filePath = imageData;
		 
         
         



function win(r) {
    console.log("Code = " + r.responseCode);
    console.log("DResponse = " + r.response);
    console.log("Sent = " + r.bytesSent);
}

function fail(error) {
    alert("An error has occurred: Code = " + error.code);
    console.log("upload error source " + error.source);
    console.log("upload error target " + error.target);
}

var uri = window.localStorage.getItem('domain')+'/api/KwtrgDB/SaveImage';
  console.log("kordy URI= " + uri); 
var options = new FileUploadOptions();
options.fileKey="Filedata";
options.fileName=imageData.substr(imageData.lastIndexOf('/') + 1);
   options.chunkedMode= false;
options.mimeType= "image/*";
   var params = new Object(); 
            params.session1 =  id+"_FirstPhoto.jpeg"; 
         
            options.params = params; 
var headers={'headerParam':'headerValue'};

options.headers = headers;

var ft = new FileTransfer();
ft.onprogress = function(progress) {
    var percent  = Math.round(progress.loaded / progress.total *360);
				   var loader = document.getElementById('b_'+ id), 
				   	   border = document.getElementById('l_'+ id),
				   		a = 0, 
						p = Math.PI; 
				   
				  (function draw() {
					  a = percent; 
					  a %= 360;
					  var r = a * p / 180, x = Math.sin(r) * 125, y = Math.cos(r) * -125, mid = a > 180 ? 1 : 0, 
					  	  anim = 'M 0 0 v -125 A 125 125 1 ' + mid + ' 1 ' + x + ' ' + y + ' z';
					 
					   loader.setAttribute('d', anim); 
					   border.setAttribute('d', anim);
					 
					if (a !== 0){
						 draw();
					} else if (a === 0){
						$('#I_'+ id).removeClass("clz_blur"); 
					}
					  
				  }()); 
};
ft.upload(imageData, uri, win, fail, options);

 
		
	}, function(err) {
    console.log("Errrrror" + err);   
        alert("Failed because: " + err);
       
    });

};




$scope.$on('$ionicView.loaded', function (event, viewData) {	
    viewData.enableBack = true;

    
   
});


/*ENDOFCAMERA*/
   
     

			$scope.$on('$ionicView.beforeEnter', function (event, viewData) {
  viewData.enableBack = true;

   
}); 
      
    /*objects*/
              
         
    /*EndOfobjects*/
             
 
           $scope.change = function(selectedItem, SelectService) {
           

         vm.City =	filterFilter(allCity, {Country_ID:parseInt(selectedItem)},true);
		     vm.Types=  filterFilter(allTypes, {Country_ID:parseInt(selectedItem), Service_ID:parseInt(SelectService) },true);   

       };

        $scope.changeService= function(selectedItem, SelectService) {

         vm.City =	filterFilter(allCity, {Country_ID:parseInt(selectedItem)},true);
		     vm.Types=  filterFilter(allTypes, {Country_ID:parseInt(selectedItem), Service_ID:parseInt(SelectService) },true);   
                     
         AddItemService.DayAutoDeleteCalculator(function(data){
		           	vm.DayCalculator  = data;             
                
                },SelectService);  

       };
       

             



            $scope.changeCity = function(selectedCity) {
            	vm.Region =	filterFilter(allRegions, {City_ID:parseInt(selectedCity)},true); 
             };       
           
              
	      

           $scope.changeType = function(selectedType) {
               
               
              AddItemService.getFeature(function(data){
		           	vm.Feature = data;             
                
                },selectedType,CountryID);  
             };  



             $scope.changeFeature = function(selectedFeature) {
                var array = selectedFeature.split(';');
                var Ucs_Id = parseInt(array[0].trim());
                var Ucs_Value = parseInt(array[1].trim()) ;         
                $filter('filter')(vm.Feature, function (d) {return d.Ucs_Id === Ucs_Id;})[0]["SelectedValue"]=Ucs_Value;      
            
              
             };  

              
        /*Submit*/
         $scope.submit = function(selectedService,selectedcountry,selectedType,selectedCity,selectedRegion,PHNumber,PHprice,PHArea,AdsTitle,AdsContent,NumCode) {
            // if (	$('#I_0').hasClass('clz_blur') || $('#I_1').hasClass('clz_blur') ||  $('#I_2').hasClass('clz_blur')|| $('#I_3').hasClass('clz_blur'))
            //   {
            //                     var alertPopup = $ionicPopup.alert({
            //                             title: 'الرجاء الانتظار',
            //                             template: ' جاري تحميل صور الاعلان الخاص بك الرجاء الانتظار و المحاولة بعد قليل في حال استمرار المشكله قم بحذف الصوره و اعادة تحميلها  للمساعدة تواصل مع الدعم الفني'
            //                             });
            //   }
            //  else{
                                    $ionicLoading.show({
                                    duration:30000,	 
                                    noBackdrop: true,
                                    template: ' <div class="spinner2">  <ion-spinner  icon="ripple" class="spinner-assertive"></ion-spinner>  </div> </ion-content>' });
                                    // alert(id);
                                    var Item = 
                                            {
                                            "Service_Id":selectedService,
                                                "Country_ID" :selectedcountry,
                                                "Type_ID": selectedType ,
                                                "City_ID":selectedCity,
                                                "Region_ID":selectedRegion,
                                                "Title":AdsTitle,
                                                "Body":AdsContent,
                                                "PhoneNo":NumCode+''+PHNumber,
                                                "Price":PHprice,
                                                "Area":PHArea,
                                                "CreatedBy":userInfo["User_ID"],
                                                "UCS":vm.Feature,
                                                "Session":id

                                            };
                                        AddItemService.InserItem (function(data){
                                               
                                          //    alert(data);
                                                        if (data==="1")
                                                        {
                                                            $state.go('app.tabs.myAds', { TypeId: 1 , serviceId:1});
                                                        }
                                                        else if (data==="10")
                                                        {
                                                            var alertPopup = $ionicPopup.alert({
                                                            title: 'لا يمكنك المتابعة ',
                                                            template: "{{ 'ExceedAds' | translate }}"
                                                            });
                                                        
                                                        }
                                                 $ionicLoading.hide({	});
                                            
                                            },Item)   ;
                        //   }
            
             }
        /*END*/     

       
	}
})();