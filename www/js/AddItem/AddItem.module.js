(function() {
	'use strict';
	
	var propertiesView = {
		templateUrl: 'js/AddItem/AddItem.html',
		controller: 'AddItemController as vm'
	};

	var EditView = {
		templateUrl: 'js/AddItem/AddItem.html',
		controller: 'EditItemController as vm'
	};


	angular
		.module('kwtrg.AddItem', [
			'ionic'
			, 'ngCordova'
			 
		])
		.config(function($stateProvider) {
			$stateProvider

			.state('app.tabs.AddItem', {
				 cache: false,
					url: '/AddItem/',
								views: { 'tab-Ads': propertiesView }
					}
				)
			
				.state('app.tabs.AddItem2', {
					cache: false,
					   url: '/AddItem/',
								   views: { 'tab-Ads': propertiesView }
					   }
				   )


	     	.state('app.tabs.EditItem', {
				 cache: false,
					url: '/EditItem/:ItemId',
								views: { 'tab-Fav': EditView }
					}
				)
			;
			
		});

		
})();
