(function() {
	'use strict';

	angular
		.module('kwtrg.AddItem')
		.factory('AddItemService', AddItemService);

	AddItemService.$inject = ['$http', '$q'];
	
	 

	/* @ngInject */
	function AddItemService($http, $q) {
		var url = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetCountriesApp?Lang='+window.localStorage.getItem('lang');
		var result = [];
		var resultCity = [];
		var resultRegion = [];
		var resultType = [];
		var FeatureType = [];
		var DayCalculatorType = [];

		var service = {
			all: all,
            allCity: allCity,
            allRegion: allRegion,
			allTypes: allTypes,
			getFeature: getFeature,
			InserItem:InserItem,
			EditItem:EditItem,
			DayAutoDeleteCalculator:DayAutoDeleteCalculator
		};
			    //ENCODE USER
				var	UserName ="";
				var Password="";
					if (window.localStorage.getItem("user") !==null)
					{
						 UserName = JSON.parse( window.localStorage.getItem('user'))["UserName"];
						 Password = JSON.parse( window.localStorage.getItem('user'))["Password"];
					}
				var encodedData = window.btoa(UserName+':'+Password); // encode a string
				var config = {
					headers : {'Authorization' : 'Basic '+encodedData}
				   };
				//END OF ENCODE
		return service;

		// *******************************************************
 
		function all(callback){
		 
			$http.get(url)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					result = data;
				    
					callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					//alert('Error : Try Again');
					console.log('ERROR (AddItem-all):' +data+ status);
					callback(result);
				});
			
			  
			 
		}


        function allCity(callback, id){
		 var urlCity = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetCitiesByCountriesApp?Lang='+window.localStorage.getItem('lang')+"&id="+id;
			$http.get(urlCity)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					resultCity = data;
				    
					callback(resultCity);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					//alert('Error : Try Again');
					console.log('ERROR (additem-allcity):'+data + status);
					callback(resultCity);
				});
	 
		}

         function allRegion(callback, id){
		 var urlCity = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetRegionsByCitiesApp?Lang='+window.localStorage.getItem('lang')+"&id="+id;
			$http.get(urlCity)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					resultRegion = data;
				    
					callback(resultRegion);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					//alert('Error : Try Again');
					console.log('ERROR (additem-allRegion):'+ data+ status);
					callback(resultRegion);
				});
			
			  
			 
		}    

              function allTypes(callback, id){
		 var urlType = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetTypeByCountryIDApp?Lang='+window.localStorage.getItem('lang')+"&id="+id;
			$http.get(urlType)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					resultType = data;
				    
					callback(resultType);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					//alert('Error : Try Again');
					console.log('ERROR (additem+allTypes):'+data + status);
					callback(resultType);
				});
			
			  
			 
		} 
			

		function getFeature(callback ,TypeID,CountryID) {
		 
		 var urlFeature = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetTypeFieldsApp?Lang='+window.localStorage.getItem('lang')+"&Type="+TypeID+"&CountryId="+CountryID;
		 
		$http.get(urlFeature)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					FeatureType = data;
				  
					callback(FeatureType);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					//alert('Error : Try Again');
					console.log('ERROR (additem-getFeature):'+data + status);
					callback(FeatureType);
				});
	}

        

	
	function InserItem(callback, Item) {
		 
		 var urlInsert = window.localStorage.getItem('domain')+'/api/KwtrgDB/InsetToItemApp';
		 
           $http({
                url: urlInsert,
                method: 'POST',
				dataType: "json",
                data:  Item,
				contentType: "application/json",
				headers : {'Authorization' : 'Basic '+encodedData}
            })
			.success(function(data, status, headers, config,serverData) {
					// this callback will be called asynchronously
					// when the response is available
					//alert( data);
					// alert("Data:-->"+ data);
				  console.log("ServerData:", serverData);
				  callback(data);
					 
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					//alert('Error : Try Again'+status+ data);
					console.log('ERROR (additem-InserItem):'+data + status);
					 
				});
                 


	}


		function EditItem(callback, Item) {
		 
		 var urlInsert = window.localStorage.getItem('domain')+'/api/KwtrgDB/EditToItemApp';
		 
           $http({
                url: urlInsert,
                method: 'POST',
				dataType: "json",
                data:  Item,
				contentType: "application/json",
                headers : {'Authorization' : 'Basic '+encodedData}
            })
			.success(function(data, status, headers, config,serverData) {
					// this callback will be called asynchronously
					// when the response is available
					//alert( data);
					
				  console.log("ServerData:", serverData);
				    
				  callback();
					 
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					//alert('Error : Try Again'+status+ data);
					console.log('ERROR (Add ITem):'+data + status);
					 
				});
                 


	}//END OF EDIT 


	function DayAutoDeleteCalculator(callback ,ServiceID) {
		 
		 var urlDayCalculatorType = window.localStorage.getItem('domain')+'/api/KwtrgDB/DayAutoDeleteCalculator?serviceID='+ServiceID;
		 
		$http.get(urlDayCalculatorType)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					DayCalculatorType = data;
				  
					callback(DayCalculatorType);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					//alert('Error : Try Again');
					console.log('ERROR (additem-getFeature):'+data + status);
					callback(DayCalculatorType);
				});
	}//end of DayAutoDeleteCalculator
	






	}
})();