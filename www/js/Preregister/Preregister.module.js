(function() {
	'use strict';
	
 


	angular
		.module('kwtrg.Preregister', [
			'ionic'
		])
		.config(function($stateProvider) {
			$stateProvider

			.state('Preregister', {
					url: '/Preregister',
					 
					templateUrl: 'js/Preregister/Preregister.html',
					controller: 'PreregisterController as vm'
				});
			;
			
		});
})();
