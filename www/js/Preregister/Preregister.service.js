(function() {
	'use strict';

	angular
		.module('kwtrg.register')
		.factory('RegisterService', RegisterService);

	RegisterService.$inject = ['$http', '$q'];
	
	 

	/* @ngInject */
	function RegisterService($http, $q) {
		
		var result = [];

		var service = {
			InserUser: InserUser,
		 
		};
		return service;

		// *******************************************************
 
		function InserUser(callback, User) {
							
							var urlInsert = window.localStorage.getItem('domain')+'/api/KwtrgDB/CreateUserApp';
							
							$http({
									url: urlInsert,
									method: 'POST',
									dataType: "json",
									data:  User,
									contentType: "application/json",
									//headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
								})
								.success(function(data, status, headers, config,serverData) {
										// this callback will be called asynchronously
										// when the response is available
								 
										
									console.log("ServerData:", serverData);
									callback(data);
										
									})
									.error(function(data, status, headers, config) {
										// called asynchronously if an error occurs
										// or server returns response with an error status.
										alert('Error : Try Again'+status+ data);
										console.log('ERROR (User):' + status);
										
									});

						}
			 
	}
})();