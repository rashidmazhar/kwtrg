(function() {
	'use strict';

	angular
		.module('kwtrg.Preregister2')
		.controller('Preregister2Controller', Preregister2Controller);

	Preregister2Controller.$inject = [
    '$state',
		 '$stateParams',
		 '$scope',
		'$timeout',
	 
	'$ionicPopup','filterFilter','$ionicLoading','$ionicSlideBoxDelegate','termService','$ionicHistory'
	];

	/* @ngInject */
	function Preregister2Controller($state,$stateParams,$scope, $timeout, $ionicPopup,filterFilter,$ionicLoading,$ionicSlideBoxDelegate,termService, $ionicHistory) {
      

		var vm = angular.extend(this, {
		 
 entries: '',
		});
      
       
			(function activate() {
              	$ionicLoading.show({			
								duration:30000,	 
								noBackdrop: true,
								template: ' <div class="spinner2">  <ion-spinner  icon="ripple" class="spinner-assertive"></ion-spinner>  </div> </ion-content>'
								});
			   termService.GetTerm(function(data){
		           	$scope.snippet = data;    
					   $ionicLoading.hide({	});         
                 
                });  
		       
		})();

		// **********************************************************

	 

 $scope.myGoBack = function() {
    $ionicHistory.goBack();
  };
	  



      }//END OF CONTROLLER    
		 
	 
	
})();