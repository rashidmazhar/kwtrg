(function() {
	'use strict';
	
 


	angular
		.module('kwtrg.Preregister2', [
			'ionic'
		])
		.config(function($stateProvider) {
			$stateProvider

			.state('Preregister2', {
					url: '/Preregister2',
					 
					templateUrl: 'js/Preregister2/Preregister2.html',
					controller: 'Preregister2Controller as vm'
				});
			;
			
		});
})();
