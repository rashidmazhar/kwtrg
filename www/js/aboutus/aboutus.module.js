(function() {
	'use strict';
	
	angular
		.module('kwtrg.aboutus', [
			'ionic'
		])
		.config(function($stateProvider) {
			$stateProvider
			.state('app.aboutus', {
					url: '/aboutus',
					views: {
						'menuContent': {
							templateUrl: 'js/aboutus/aboutus.html',
						 controller: 'aboutusController as vm'
						}
					}
				}); 
				
		});
})();
