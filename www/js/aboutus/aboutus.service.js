(function() {
	'use strict';

	angular
		.module('kwtrg.aboutus')
		.factory('aboutusService', aboutusService);

	aboutusService.$inject = ['$http', '$q'];
	
	 

	/* @ngInject */
	function aboutusService($http, $q) {
		//ENCODE USER
		var	UserName ="";
		var Password="";
			if (window.localStorage.getItem("user") !==null)
			{
				 UserName = JSON.parse( window.localStorage.getItem('user'))["UserName"];
				 Password = JSON.parse( window.localStorage.getItem('user'))["Password"];
			}
		var encodedData = window.btoa(UserName+':'+Password); // encode a string
		var config = {
			headers : {'Authorization' : 'Basic '+encodedData}
		   };
		//END OF ENCODE
		var url = window.localStorage.getItem('domain')+'/api/KwtrgDB/getTerms?Lang='+window.localStorage.getItem('lang');
		var result = [];

		var service = {
			Getaboutus: Getaboutus,
		 
		};
		return service;

		// *******************************************************
 
		function Getaboutus(callback){
		 
			$http.get(url,config)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					result = data;
				 
					callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					alert('no');
					console.log('ERROR (aboutus):' + status);
					callback(result);
				});
			
			  
			 
		}
		
		

		 
	}
})();