// Ionic Starter App
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js

var appVersion = "0.0.0";
angular
  .module("kwtrg", [
    "ionic",
    "ionic.native",
    "ionicImgCache",
    "kwtrg.Notify",
    "kwtrg.Bo3qar",
    "kwtrg.term",
    "ionic-material",
    "ngCordova",
    "config",
    "kwtrg.home",
    "kwtrg.main",
    "kwtrg.news",
    "kwtrg.menu",
    "kwtrg.tabs",
    "kwtrg.AddItem",
    "kwtrg.AddBo3qar",
    "kwtrg.request",
    "kwtrg.gallary",
    "kwtrg.login",
    "kwtrg.share",
    "kwtrg.myAds",
    "kwtrg.register",
    "kwtrg.Preregister",
    "kwtrg.Preregister2",

    "kwtrg.slider",
    ,
    "ion-fab-button",
    "kwtrg.aboutus",
    "ng-walkthrough",
    "ionic.native"
  ])

  // 'dcbImgFallback'

  .run(function(
    $state,
    $ionicPlatform,
    $rootScope,
    $timeout,
    $ionicHistory,
    $ionicSlideBoxDelegate,
    $location,
    $ionicPopup,
    $translate,
    $cordovaDeeplinks,
    $http,
    $q
  ) {
    // window.localStorage.setItem("domain", "http://localhost:61403/");
    window.localStorage.setItem("domain", "http://kwtrgtest.azurewebsites.net");

    $rootScope.new_list = [
      { id: "5", img: "img/yeoman-{{locale}}.png" },
      { id: "1", img: "img/001.png" },
      { id: "2", img: "img/002.png" },
      { id: "3", img: "img/003.png" },
      { id: "4", img: "img/004.png" }
    ];
    $rootScope.list = [
      { id: "1", img: "img/01.png" },
      { id: "2", img: "img/001.png" },
      { id: "3", img: "img/det.png" },
      { id: "1", img: "img/01.png" },
      { id: "2", img: "img/001.png" },
      { id: "3", img: "img/det.png" }
    ];
    $ionicPlatform.ready(function() {
      var permissions = cordova.plugins.permissions;

      permissions.requestPermission(permissions.CAMERA, success, error);
      permissions.requestPermission(
        permissions.READ_EXTERNAL_STORAGE,
        success,
        error
      );
      permissions.requestPermission(
        permissions.WRITE_EXTERNAL_STORAGE,
        success,
        error
      );

      function error() {
        console.warn("Camera permission is not turned on");
      }

      function success(status) {
        if (!status.hasPermission) error();
      }

      permissions.hasPermission(permissions.READ_EXTERNAL_STORAGE, function(
        status
      ) {
        if (status.hasPermission) {
          console.log("Yes :D ");
        } else {
          console.warn("No :( ");
        }
      });

      console.log("status kordi --> " + StatusBar);

      // StatusBar.styleLightContent();
      StatusBar.overlaysWebView(false);
      // StatusBar.backgroundColorByName("red");
      StatusBar.backgroundColorByHexString("#f6921e");

      //Push
      //push notificatio

      window.FirebasePlugin.grantPermission();

      //   window.FirebasePlugin.hasPermission(function(data){
      //     console.log( "notification permission :" + data.isEnabled);
      // });

      window.FirebasePlugin.getToken(
        function(token) {
          // save this server-side and use it to push notifications to this device
          console.log("notification toke : " + token);
          //check the previous Token and call web api to insert in case it's different
          var oldRegId = localStorage.getItem("registrationId");
          var UserID = "";
          var ios = 0;
          if (1 === 1) {
            //if (oldRegId !== token) {
            if (window.localStorage.getItem("user") !== null) {
              console.log("This Token for register user  ");
              UserID = JSON.parse(window.localStorage.getItem("user"))[
                "User_ID"
              ];
            }
            if (ionic.Platform.platform() === "ios") {
              console.log(" notification Set Value for register  IOS  ");
              ios = 1;
            } else {
              console.log(" notification Set Value for register  Android  ");
            }
            console.log("Calling Notification Service 2");
            // InserToken( function(data){
            //   console.log(data);
            //   if (data==="true")
            //   {
            //     localStorage.setItem('registrationId', token);
            //   }
            // } ,token ,ios ,UserID )

            var urlInsert =
              window.localStorage.getItem("domain") +
              "/api/KwtrgDB/GetToken?_userId=" +
              UserID +
              "&_token=" +
              token +
              "&_ios=" +
              ios;
            //alert(urlInsert);
            console.log("repo0 :  " + urlInsert);
            $http({
              url: urlInsert,
              method: "GET",
              dataType: "json",

              contentType: "application/json"
              //headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
              .success(function(data, status, headers, config, serverData) {
                // this callback will be called asynchronously
                // when the response is available
                //alert( data);
                console.log("Data:-->" + data);
                console.log("ServerData:", serverData);
                if (data === "true") {
                  console.log("registrationId", token);
                  localStorage.setItem("registrationId", token);
                } else {
                  console.log("No registrationId", token);
                }
              })
              .error(function(data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                //alert('Error : Try Again'+status+ data);
                console.log(
                  "ERROR (Notification not inserted ):" + data + status
                );
              });
          }
        },
        function(error) {
          console.error("notification error get toke : " + error);
        }
      );

      window.FirebasePlugin.onNotificationOpen(
        function(notification) {
          //console.log( "notification" + notification.Type);

          var type = notification.Type;
          console.log(type);
          console.log(type == "Notify");
          console.log("ID" + notification.ID);

          //$state.go('app.article', { articleId: articleId });
          if (type == "Notify") {
            $state.go("app.notify", { notifyId: notification.ID });
          } else if (type == "Bo3qar") {
            // $state.go('app.notify',{ notifyId: notification.ID  });
            $state.go("app.article", {
              articleId: notification.ID,
              IsBo3qar: "1"
            });
          }
          //alert("notification" + notification.ReadID  );
        },
        function(error) {
          console.error("notification error :" + error);
        }
      );
      //end of push
      //endofpush

      //register();

      // Note: route's first argument can take any kind of object as its data,
      // and will send along the matching object if the route matches the deeplink
      $cordovaDeeplinks
        .route({
          "/details/:articleId": {
            target: "app.article",
            parent: "app.tabs.main"
          },
          "/items/details/:articleId": {
            target: "app.article",
            parent: "app.tabs.main"
          },
          "/Home": {
            target: "app.aboutus",
            parent: "app.tabs.main"
          }
        })
        .subscribe(
          function(match) {
            console.log("url match");
            // One of our routes matched, we will quickly navigate to our parent
            // view to give the user a natural back button flow
            $timeout(function() {
              console.log(" match parent");
              $state.go(match.$route.parent, match.$args);

              // Finally, we will navigate to the deeplink page. Now the user has
              // the 'product' view visibile, and the back button goes back to the
              // 'products' view.
              $timeout(function() {
                console.log(" match target");
                $state.go(match.$route.target, match.$args);
              }, 800);
            }, 100); // Timeouts can be tweaked to customize the feel of the deeplink
          },
          function(nomatch) {
            console.warn("No match", nomatch);
          }
        );

      //  if(window.Connection) {
      //           if(navigator.connection.type == Connection.NONE) {
      //               $ionicPopup.confirm({
      //                   title: "Internet Disconnected !",
      //                   content: " {{ 'ConnectionFailed' | translate }}"
      //               })
      //               .then(function(result) {
      //                   if(!result) {
      //                       ionic.Platform.exitApp();
      //                   }
      //               });
      //           }
      //       }

      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
      }
      if (window.StatusBar) StatusBar.styleDefault();
      $timeout(function() {
        if (navigator.splashscreen) navigator.splashscreen.hide();
      }, 3000);

      cordova.getAppVersion(function(version) {
        $rootScope.appVersion = version;
      });
    });
    $rootScope.myGoBack = function() {
      $ionicHistory.goBack();
    };
    $rootScope.goto = function(url) {
      $location.path(url);
    };

    $rootScope.active_icon = function(type) {
      if (type == "fav")
        $rootScope.coloractive_fav = !$rootScope.coloractive_fav;
      else if (type == "mark")
        $rootScope.coloractive_mark = !$rootScope.coloractive_mark;
      else if (type == "font")
        $rootScope.coloractive_font = !$rootScope.coloractive_font;
      else if (type == "share")
        $rootScope.coloractive_share = !$rootScope.coloractive_share;
    };
    $rootScope.forget_password = function() {
      $ionicPopup.show({
        template:
          'Enter your email address below.<label class="item item-input" style="height: 34px; margin-top: 10px;">' +
          '<input  type="email"  /></label>',
        title: "Forget Password",
        subTitle: " ",
        scope: $rootScope,
        buttons: [
          { text: "<b>Send</b>", type: "button-positive" },
          { text: "Cancel", type: "button-positive" }
        ]
      });
    };

    $rootScope.InserToken = function(callback, _token, _ios, _userId) {
      var urlInsert =
        window.localStorage.getItem("domain") +
        "/api/KwtrgDB/GetToken?_userID=" +
        _userId["User_ID"] +
        "&_token=" +
        _token +
        "&_ios=" +
        _ios;

      console.log(urlInsert);
      $http({
        url: urlInsert,
        method: "GET",
        dataType: "json",

        contentType: "application/json"
        //headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      })
        .success(function(data, status, headers, config, serverData) {
          // this callback will be called asynchronously
          // when the response is available
          //alert( data);
          // alert("Data:-->"+ data);
          console.log("ServerData:", serverData);
          callback(result);
        })
        .error(function(data, status, headers, config) {
          // called asynchronously if an error occurs
          // or server returns response with an error status.
          //alert('Error : Try Again'+status+ data);
          console.log("ERROR (additem-InserItem):" + data + status);
        });
    };

    $rootScope.nextSlide = function() {
      $ionicSlideBoxDelegate.$getByHandle("lang").next();
    };

    $rootScope.previousSlide = function() {
      $ionicSlideBoxDelegate.$getByHandle("lang").previous();
    };

    $rootScope.nextSlide2 = function() {
      $ionicSlideBoxDelegate.$getByHandle("CountrySlide").next();
    };

    $rootScope.previousSlide2 = function() {
      $ionicSlideBoxDelegate.$getByHandle("CountrySlide").previous();
    };

    var lang = window.localStorage.getItem("lang");

    if (lang) {
      //$urlRouterProvider.otherwise('/login');
      $translate.use(lang);
      //This is a second time launch, and count = applaunchCount
    } else {
      //Local storage is not set, hence first time launch. set the local storage item
      $translate.use("ar");
      //Do the other stuff related to first time launch
    }
  })

  .config(function($urlRouterProvider, $ionicConfigProvider) {
    $ionicConfigProvider.backButton.text("").previousTitleText("");
    $ionicConfigProvider.tabs.position("bottom");
    $ionicConfigProvider.views.swipeBackEnabled(true);
    // if none of the above states are matched, use this as the fallback
    var applaunchCount = window.localStorage.getItem("launchCount");

    //Check if it already exists or not
    if (applaunchCount >= 1) {
      //$urlRouterProvider.otherwise('/login');

      window.localStorage.setItem(
        "launchCount",
        parseInt(applaunchCount) + parseInt("1")
      );
      $urlRouterProvider.otherwise("/app/tabs/main");
      //This is a second time launch, and count = applaunchCount
    } else {
      //Local storage is not set, hence first time launch. set the local storage item
      window.localStorage.setItem("launchCount", 1);
      $urlRouterProvider.otherwise("/home");
      window.localStorage.setItem("mainTour", 1);
      window.localStorage.setItem("ItemsTour", 1);
      window.localStorage.setItem("ItemsDetail", 1);
      window.localStorage.setItem("GallaryTour", 1);
      window.localStorage.setItem("myAds", 1);
      //Do the other stuff related to first time launch
    }
  });
