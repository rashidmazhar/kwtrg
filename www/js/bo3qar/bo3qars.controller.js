(function() {
	'use strict';

	angular
		.module('kwtrg.Bo3qar')
		.controller('Bo3qarsController', Bo3qarsController);

	Bo3qarsController.$inject = ['$rootScope','$scope', '$state','$ionicPopup', 'Bo3qarService',  'ionicMaterialMotion','$stateParams','$filter','$ionicLoading','AddBo3qarService'];



 




	/* @ngInject */
	function Bo3qarsController($rootScope,$scope, $state,$ionicPopup, Bo3qarService,ionicMaterialMotion,$stateParams,$filter,$ionicLoading,AddBo3qarService) {
		var vm = angular.extend(this, {
			bo3qars: [],
			deleteReq:deleteReq,
			navigate: navigate,
		 
			Country: [],
			sortBy: 'Date',
			selectedCategory: '1',
			doRefresh: doRefresh,
			CountryID: 1, 
			TypeId:parseInt($stateParams.TypeId),
			serviceID:parseInt($stateParams.serviceId),
			UserID:1,
		
		 
			City: [],
			 
			Region: [],
			Feature:[],
			Types:[],
			somePlaceholder:$filter('translate')('Search2'),

		});

  
 



	  
		$scope.$on('$ionicView.loaded', function (event, viewData) {	
			viewData.enableBack = false;
													  
												   
   
			  }); 
		

	
		$scope.$on('$ionicView.beforeEnter', function (event, viewData) {
			if (window.localStorage.getItem("user") !==null)
      {
        $state.go('app.tabs.myAds');
	  }
	  else{
			
		

		viewData.enableBack = false;

		$ionicLoading.show({
								   
			duration:30000,	 
			noBackdrop: true,
			template: ' <div class="spinner2">  <ion-spinner  icon="ripple" class="spinner-assertive"></ion-spinner>  </div> </ion-content>'
			});
		 

//	AddBo3qarService.HaveActiveRequest (function(data){
											
				// if (data==="1")
				// {
					Bo3qarService.all(function(data){
						vm.bo3qars = data;	 
					   
						$ionicLoading.hide({	});
					});
					//$state.go('app.tabs.bo3qars'); //This mean anonymous user a=with acitve request
				// }
				// else 
				// {
				// 	$ionicLoading.hide({	});
				// 	$state.go('app.tabs.AddBo3qar'); //This is anonymous user new request
				// }
	
		
											 // 	});
	  }
						
												
								
							
							 
						}); 


	 
		// ********************************************************************

        var TypeId = parseInt($stateParams.TypeId);
		var serviceID = parseInt($stateParams.serviceId);
	    var userInfo=-1;
		var searchText ='';
		  var CountryID =parseInt(window.localStorage.getItem('Country_ID'));
      

			(function activate() {
				 
			   
  	         	 
			
		})();


	

		// function navigate(bo3qarId) {
			 
		// 	$state.go('app.bo3qar', { bo3qarId: bo3qarId });
		// 	console.log("Call Bo3qar Page");
		// }
 
		function navigate(bo3qarId) {
			$state.go('app.article', { articleId: bo3qarId , IsBo3qar:'1' });
		}

		function deleteReq()
		{
			 
		 
				var confirmPopup = $ionicPopup.confirm({
						title: 'رسالة تاكيد  ',
					template: "{{ 'SureMsgReq' | translate }}"   
				});
	
				confirmPopup.then(function(res) {
					if(res) {
					console.log('You are sure-->');
					$ionicLoading.show({
						duration:60000,	 
						noBackdrop: true,
						template: ' <div class="spinner2">  <ion-spinner  icon="ripple" class="spinner-assertive"></ion-spinner>  </div> </ion-content>' });
						// alert(id);
					Bo3qarService.deletereq(function(data){
						$ionicLoading.hide({	});
						 
						$state.go('app.tabs.main');
					});
					} else {
					console.log('You are not sure');
					}
				});
			 
		}
 
		

		 
		function doRefresh() {
		setTimeout(
			 		
				Bo3qarService.all(function(data){
				vm.bo3qars = data;
				$scope.$broadcast('scroll.refreshComplete');
			}, TypeId,serviceID)

		, 30000)
		
		}

 
		 
	}//Controller
})();