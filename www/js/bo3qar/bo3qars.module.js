(function() {
	'use strict';

	var propertiesView = {
		templateUrl: 'js/bo3qar/bo3qars.html',
		controller: 'Bo3qarsController as vm'
	};


	angular
		.module('kwtrg.Bo3qar', [
			'ionic',
			'angucomplete-alt'
			 
		])
		.config(function($stateProvider) {
			$stateProvider
	 
             
				.state('app.tabs.bo3qars', {
					cache: false,
					url: '/bo3qars', 	views: { 'tab-Fav': propertiesView }
					
				 
				})


				.state('app.tabs.bo3qars2', {
					cache: false,
					url: '/bo3qars/', 	views: { 'tab-Fav': propertiesView }
					
				 
				})
			

				.state('app.bo3qar', {
				 	 cache: false,
					url: '/bo3qar/:bo3qarId',
					views: {
						'menuContent': {
							templateUrl: 'js/bo3qar/bo3qar.html',
							controller: 'Bo3qarController2 as vm'
						}
					}
				});
		});
})();