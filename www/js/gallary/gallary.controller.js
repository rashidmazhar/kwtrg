(function() {
  "use strict";

  angular
    .module("kwtrg.gallary")
    .controller("GallaryController", GallaryController);

  GallaryController.$inject = [
    "$stateParams",
    "menuItems",

    "$timeout",

    "ionicMaterialMotion",
    "gallaryService",
    "filterFilter",
    "$scope",
    "$rootScope",
    "$ionicSlideBoxDelegate",
    "$ionicLoading"
  ];

  /* @ngInject */
  function GallaryController(
    $stateParams,
    menuItems,
    $timeout,
    ionicMaterialMotion,
    gallaryService,
    filterFilter,
    $scope,
    $rootScope,
    $ionicSlideBoxDelegate,
    $ionicLoading
  ) {
    var vm = angular.extend(this, {
      entries: [],
      serviceId: parseInt($stateParams.serviceId),

      showAccountsWalkthrough: false,
      nextAccountsWalkthrough: nextAccountsWalkthrough,
      getAdd: getGalleryAdd,
      slider: [],
      valid_elements: [],
      Count: 0,
      skip_count: 0
    });

    //TOUR
    var mainTour = window.localStorage.getItem("GallaryTour");

    if (mainTour == 1 || mainTour === null || !angular.isDefined(mainTour)) {
      setTimeout(function() {
        vm.showAccountsWalkthrough = true;
        window.localStorage.setItem("GallaryTour", 0);
      }, 500);
    }

    $scope.accountsWalkthrough2 = {
      steps: [
        {
          text: "img/dep.png"
        },

        {
          text: "img/backa.png"
        }
      ],
      currentIndex: 0
    };

    function nextAccountsWalkthrough() {
      ionic.requestAnimationFrame(function() {
        if (
          $scope.accountsWalkthrough2.currentIndex <
          $scope.accountsWalkthrough2.steps.length - 1
        ) {
          $scope.accountsWalkthrough2.currentIndex++;
          vm.showAccountsWalkthrough = true;
        }
      });
    }
    //ENDOFTOUR
    function getGalleryAdd(type, country) {
      gallaryService.getGalleryAdd(type, country, function(data) {
        if (data.status === "OK") {
          vm.slider = data.result.filter(function(ad, index) {
            var now = new Date();
            var from = new Date(ad.date_from);
            var to = new Date(ad.date_to);
            if (now > from && now < to) {
              return true;
            } else false;
          });
          var counter = 0;
          var next_slide = function() {
            var scroll_time =
              vm.valid_elements[counter].slider_scroll === 0
                ? 1000
                : vm.valid_elements[counter].slider_scroll;

            $ionicSlideBoxDelegate.$getByHandle("IOBOX").next(1000);
            $ionicSlideBoxDelegate.update();
            $ionicSlideBoxDelegate.loop(true);

            //console.log("HERE");
            counter++;
            if (counter === vm.valid_elements.length) counter = 0;

            setTimeout(next_slide, scroll_time * 1000);
          };

          $ionicLoading.show({
            duration: 3000,
            noBackdrop: true,
            template:
              ' <div class="spinner2">  <ion-spinner  icon="ripple" class="spinner-assertive"></ion-spinner>  </div> </ion-content>'
          });

          data.result.forEach(element => {
            //console.log(element);
            if (element.valid_ad === true) vm.valid_elements.push(element);
          });

          next_slide();
          vm.statusad = true;
          console.log("/********************SLIDER*************************/");
        } else {
          vm.statusad = false;
        }
      });
    }

    (function activate() {
      var Gallary = JSON.parse(window.localStorage.getItem("Gallary"));
      var CountryID = parseInt(window.localStorage.getItem("Country_ID"));
      vm.getAdd(vm.serviceId, CountryID);
      if ($ionicSlideBoxDelegate._instances[0] !== undefined) {
        vm.slider = [];
        vm.valid_elements = [];
        vm.statusad = false;
        $ionicSlideBoxDelegate._instances[0].kill();
        $ionicSlideBoxDelegate.update();
      }
      vm.entries = filterFilter(
        Gallary,
        { Country_ID: CountryID, Service_ID: parseInt($stateParams.serviceId) },
        true
      );
      // vm.entries=  filterFilter(Gallary, {Service_ID:parseInt($stateParams.serviceId)},true);
      $timeout(function() {
        ionicMaterialMotion.fadeSlideInRight({
          startVelocity: 2500
        });
      }, 400);
    })();

    // **********************************************************
  }
})();
