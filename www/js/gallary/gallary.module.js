(function() {
	'use strict';
	
	var propertiesView = {
		templateUrl: 'js/gallary/gallary.html',
		controller: 'GallaryController as vm'
	};


	angular
		.module('kwtrg.gallary', [
			'ionic'
		])
		.config(function($stateProvider) {
			$stateProvider

			.state('app.tabs.gallary', {
			
					url: '/gallary/:serviceId',
								views: { 'tab-home': propertiesView }
					}
				)
			;
			
		});
})();
