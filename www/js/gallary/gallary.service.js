(function() {
  "use strict";

  angular.module("kwtrg.gallary").factory("gallaryService", gallaryService);

  gallaryService.$inject = ["$http", "$q"];

  /* @ngInject */
  function gallaryService($http, $q) {
    var url =
      window.localStorage.getItem("domain") +
      "/api/KwtrgDB/GetTypeByCountryIDApp?id=1&Lang=" +
      window.localStorage.getItem("lang");
    var result = [];

    var service = {
      all: all,
      get: get,
      getGalleryAdd: getGalleryAdd
    };
    return service;

    // *******************************************************
    function getGalleryAdd(type, country, callback) {
      var url =
        window.localStorage.getItem("domain") +
        "api/KwtrgDB/GetAd/" +
        type +
        "/" +
        country;
      $http
        .get(url)
        .success(function(data, status, headers, config) {
          // this callback will be called asynchronously
          // when the response is available
          result = data;

          callback(result);
        })
        .error(function(data, status, headers, config) {
          // called asynchronously if an error occurs
          // or server returns response with an error status.
          //alert('no');
          console.log("ERROR (gallary):" + data + status);
          callback(result);
        });
    }
    function all(callback) {
      $http
        .get(url)
        .success(function(data, status, headers, config) {
          // this callback will be called asynchronously
          // when the response is available
          result = data;

          callback(result);
        })
        .error(function(data, status, headers, config) {
          // called asynchronously if an error occurs
          // or server returns response with an error status.
          //alert('no');
          console.log("ERROR (gallary):" + data + status);
          callback(result);
        });
    }

    function get(articleId) {
      // we take an article from cache but we can request ir from the server
      for (var i = 0; i < result.length; i++) {
        if (result[i].id === articleId) {
          return $q.when(result[i]);
        }
      }
      return $q.when(null);
    }
  }
})();
