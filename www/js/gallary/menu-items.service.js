(function() {
	'use strict';

	angular
		.module('kwtrg.gallary')
		.factory('menuItems', menuItems);

	menuItems.$inject = [];

	/* @ngInject */
	function menuItems() {
		var data = [{
			title: 'شقق و منازل',
			path: 'articles',
			icon: 'ion-ios-home'
		}, {
			title: 'ادوار',
			path: 'products',
			icon: 'ion-podium'
		}, {
			title: 'Galleries',
			path: 'galleries',
			icon: 'ion-images'
		}, {
			title: 'قطعة ارض',
			path: 'map',
			icon: 'ion-map'
		}];

		return data;
	}
})();