(function() {
	'use strict';

	angular
		.module('kwtrg.home')
		.controller('HomeController', HomeController);

	 	HomeController.$inject = [
		'$rootScope','$scope', '$translate' , 'homeDataService','$ionicLoading'
	];

	/* @ngInject */
	function HomeController(
		$rootScope,$scope, $translate , homeDataService,$ionicLoading) {
			 
		 

			
           window.localStorage.setItem('Country_ID',1);//Default value
           $scope.firstSlideChanged = function(index) {
			switch(index) {
			case 0:
			 $scope.ChangeLanguage('ar');
			 
			break;
			case 1:
			$scope.ChangeLanguage('en');
			break;
			}
			};
           //Secound
		   $scope.slideChanged = function(index) {
			switch(index) {
			case 0:
			window.localStorage.setItem("Country_ID", index+1);
			console.log('Kuwait-->'+ window.localStorage.getItem('Country_ID'));
			break;
			case 1:
			window.localStorage.setItem("Country_ID", 2);
			console.log('UAE-->'+ window.localStorage.getItem('Country_ID'));
			break;
				case 2:
			window.localStorage.setItem("Country_ID", 4);
			console.log('Egypt-->'+ window.localStorage.getItem('Country_ID'));
			break;
				case 3:
			window.localStorage.setItem("Country_ID", 6);
			console.log('Qatar-->'+ window.localStorage.getItem('Country_ID'));
			break;
				case 4:
			window.localStorage.setItem("Country_ID", 7);
			console.log('Bahrain-->'+ window.localStorage.getItem('Country_ID'));
			break;
				case 5:
			window.localStorage.setItem("Country_ID", 8);
			console.log('Omman-->'+ window.localStorage.getItem('Country_ID'));
			break;
				case 6:
			window.localStorage.setItem("Country_ID", 9);
			console.log('KSA-->'+ window.localStorage.getItem('Country_ID'));
			break;
			}
			};
           
			$scope.ChangeLanguage = function(lang){
			$translate.use(lang);
			
			window.localStorage.setItem("lang", lang);
			};

			$scope.ChangeLanguageSlide = function(lang){
				$translate.use(lang);
			
				if (lang=="en")
				{
					 
				   $rootScope.nextSlide();
				}
				else
				{
				 
					$rootScope. previousSlide();
				
				}
			window.localStorage.setItem("lang", lang);
			};

			$scope.ChangeCountrySlide = function(lang){
			 
				if (lang=="nxt")
				{
					 
				   $rootScope.nextSlide2();
				}
				else
				{
				 
					$rootScope. previousSlide2();
				
				}
			 
			};


			//CALLING SERVERICES//
             		(function activate() {
		                     
							//  homeDataService.allFeature(function(data){ window.localStorage.setItem("allFeature",  JSON.stringify(data)); });  
	                	})();

			 
		
			 	$scope.$on('$ionicView.beforeLeave', function (event, viewData) {

					       
						   
					     	$ionicLoading.show({			
								duration:30000,	 
								noBackdrop: true,
								template: ' <div class="spinner2">  <ion-spinner  icon="ripple" class="spinner-assertive"></ion-spinner>  </div> </ion-content>'
								});
                         
                              homeDataService.Types(function(data){ window.localStorage.setItem("Gallary",  JSON.stringify(data)); });
							  homeDataService.allCountry(function(data){ window.localStorage.setItem("allCountry",  JSON.stringify(data)); });
							  homeDataService.allCity(function(data){ window.localStorage.setItem("allCity",  JSON.stringify(data)); });
                              homeDataService.allRegion(function(data){  $ionicLoading.hide({	}); window.localStorage.setItem("allRegion",  JSON.stringify(data)); });



						}); 

			//END OF CALLING//
			
	}
	
	
})();