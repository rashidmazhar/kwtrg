(function() {
	'use strict';

	angular
		.module('kwtrg.home', [
			'ionic',
			 	'ngCordova',
			
			'pascalprecht.translate'
		])
		
		
		
		
		
		.config(function($stateProvider, $urlRouterProvider,$translateProvider) {
			
			  $translateProvider.useStaticFilesLoader({
				prefix: 'languages/locale-',
				suffix: '.json'
			});

				var lang = window.localStorage.getItem('lang');
				if(lang){
				$translateProvider.preferredLanguage('lang');
				}
				else
				{
				$translateProvider.preferredLanguage('ar');
				}
	
 				 $translateProvider.useLoaderCache(true);
			
				$stateProvider
					.state('home', {
						url: '/home',
					templateUrl: 'js/home/home.html',
					controller: 'HomeController'
							
						 
					});
				
				
		});
		
		
		
		
		
})();