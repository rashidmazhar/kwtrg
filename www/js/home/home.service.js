(function() {
	'use strict';

	angular
		.module('kwtrg.home')
		.factory('homeDataService', homeDataService);

	homeDataService.$inject = ['$http', '$q'];
 

	/* @ngInject */
	function homeDataService($http, $q) {
 

        var result = [];
 var Lang=window.localStorage.getItem('lang');  
		var service = {
			Types: Types,
			allCountry:allCountry,
			allCity:allCity,
			allRegion:allRegion,
			allFeature:allFeature,
			 
		};
		return service;


		/**************************CASHING VALUES********************************/
    

 	function Types(callback){
			  Lang=window.localStorage.getItem('lang');  
			 // alert(window.localStorage.getItem('domain'));
		 	var urlType = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetTypeByCountryIDAppAllOrder?&Lang='+Lang;
			$http.get(urlType)
				.success(function(data, status, headers, config) {
					result = data;		 
					callback(result);
				})
				.error(function(data, status, headers, config) {
				//	alert('no');
					console.log('ERROR Check Internet Connection (gallary):' + status);
					callback(result);
				});  			 
		}


        

	function allCountry(callback){
		   Lang=window.localStorage.getItem('lang');  
			var urlCountry = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetCountriesApp?Lang='+Lang;
				$http.get(urlCountry)
					.success(function(data, status, headers, config) {
						result = data;
						callback(result);
					})
					.error(function(data, status, headers, config) {
						//alert('Error : Try Again');
						console.log('ERROR Check Internet Connection (Country):' + status);
						callback(result);
					});				  	 
			}



	function allCity(callback){
		   Lang=window.localStorage.getItem('lang');  
		    var urlCity = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetCitiesAll?Lang='+Lang;
			$http.get(urlCity)
				.success(function(data, status, headers, config) {
					result = data; 
					callback(result);
				})
				.error(function(data, status, headers, config) {
					//alert('Error : Try Again');
					console.log('ERROR Check Internet Connection (Country):' + status);
					callback(result);
				});	 
		}


   function allRegion(callback){
	    Lang=window.localStorage.getItem('lang');  
		 var urlCity = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetRegionsAppAll?Lang='+Lang;
			$http.get(urlCity)
				.success(function(data, status, headers, config) {
					result = data;		    
					callback(result);
				})
				.error(function(data, status, headers, config) {
					//alert('Error : Try Again');
					console.log('ERROR Check Internet Connection (Country):' + status);
					callback(result);
				});
	        }


	function allFeature(callback) {		
		 Lang=window.localStorage.getItem('lang');   
    	 var urlFeature = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetTypeFieldsAppAll?Lang='+Lang;
		 $http.get(urlFeature)
				.success(function(data, status, headers, config) {
					result = data;
					callback(result);
				})
				.error(function(data, status, headers, config) {
					//alert('Error : Try Again');
					console.log('ERROR (Feature):' + status);
					callback(result);
				});
	       }



	}

})();