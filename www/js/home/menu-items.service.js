(function() {
	'use strict';

	angular
		.module('kwtrg.home')
		.factory('menuItems', menuItems);

	menuItems.$inject = [];

	/* @ngInject */
	function menuItems() {
		var data = [{
			title: 'اخبار',
			path: 'articles',
			icon: 'ion-speakerphone'
		}, {
			title: 'Products',
			path: 'products',
			icon: 'ion-bag'
		}, {
			title: 'Galleries',
			path: 'galleries',
			icon: 'ion-images'
		}, {
			title: 'Map',
			path: 'map',
			icon: 'ion-map'
		}];

		return data;
	}
})();