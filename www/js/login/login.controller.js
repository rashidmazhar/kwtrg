(function() {
	'use strict';

	angular
		.module('kwtrg.login')
		.controller('loginController', loginController);

	loginController.$inject = [
		 '$scope',
		 '$ionicPopup',
		 '$state',
	 '$rootScope',
		'loginService'
	];

	/* @ngInject */
	function loginController($scope,$ionicPopup, $state,$rootScope,  loginService) {

		var vm = angular.extend(this, {
			entries: [],
	     	username:'', 
			 password:''
		 
			 
		});



		 $scope.login = function() {
            
			  loginService.loginUser(function(data){
			vm.entries = data;
			 
			 if (data===null)
			 {
				  IsActive =undefined;
			 }
			 else
			 {
			var IsActive = data["IsActive"];
			}

			//alert(IsActive);

			switch (IsActive) {
            case undefined:
                var alertPopup = $ionicPopup.alert({ 
                title: "خطأ بتسجيل الدخول" ,    
                template: "{{ 'credentials' | translate }}"
                 });
				 // window.localStorage.removeitem("user");
				  delete localStorage['user'];
                break;
            
             case 99:
                var alertPopup = $ionicPopup.alert({ 
                title: "تم حذف المستخدم" ,    
                template: "{{ 'credentialsDel' | translate }}"
                 });
				 // window.localStorage.removeitem("user");
				  delete localStorage['user'];
                break;

             case 0:
			  var alertPopup = $ionicPopup.alert({
                title: '! مستخدم غير مفعل ',
                template:data["FirstName"]+"{{ 'credentialsAct' | translate }}"   
                 });
				 window.localStorage.setItem("user", JSON.stringify(data));
				$rootScope.myGoBack();
				break;

            default:
			  var alertPopup = $ionicPopup.alert({
                title: '! تم تسجيل الدخول بنجاح ',
                template:data["FirstName"]+"{{ 'Welcome' | translate }}"   
				 });
				 data["Password"]=vm.password;
				 data["UserName"]=vm.username;
				 window.localStorage.setItem("user", JSON.stringify(data));
				
				 loginService.getToken(data);
				$rootScope.myGoBack();

        }
			 
		},vm.username, vm.password);


        
    }




		 $scope.logout = function() {
			 
                 var alertPopup = $ionicPopup.alert({ 
                title: "تسجيل الخروج" ,    
                template: "{{ 'logout' | translate }}"
                 });
				 // window.localStorage.removeitem("user");
				 if (window.localStorage.getItem("user") !==null)
				 {
					var	UserID = JSON.parse( window.localStorage.getItem('user'))["User_ID"]
					
					// var oldRegId = localStorage.getItem('registrationId');
					// if (oldRegId !==null) {
					// 	console.log(' remove token  ' + oldRegId  );
					// 	//CALL WEB API DELTE FROM REGISTER TOKEN TO anonymous
					// 	delete localStorage['registrationId'];
					// }
				 
				   // Post registrationId to your app server as the value has changed
				 }
				  delete localStorage['user'];
			  loginService.getToken(null);
               
                    }
	
       

	 
		 
	 
	}
})();