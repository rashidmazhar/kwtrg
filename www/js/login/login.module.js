(function() {
	'use strict';
	
 


	angular
		.module('kwtrg.login', [
			'ionic'
		])
		.config(function($stateProvider) {
			$stateProvider

			.state('login', {
					url: '/login',
					 
					templateUrl: 'js/login/login.html',
					controller: 'loginController as vm'
				});
			 
			;
			
		});
})();
