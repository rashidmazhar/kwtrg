(function() {
	'use strict';

	angular
		.module('kwtrg.main')
		.factory('mainService', mainService);

	mainService.$inject = ['$http', '$q'];
	
	 

	/* @ngInject */
	function mainService($http, $q) {
		var url = window.localStorage.getItem('domain')+'/api/KwtrgDB/getVersion' ;
		var result = [];

		var service = {
			getVersion: getVersion,
			getDataVersion : getDataVersion,
			getPopupAd: getPopupAd,
			getSliderAd: getSliderAd
			 
		};

		  //ENCODE USER
				var	UserName ="";
				var Password="";
					if (window.localStorage.getItem("user") !==null)
					{
						 UserName = JSON.parse( window.localStorage.getItem('user'))["UserName"];
						 Password = JSON.parse( window.localStorage.getItem('user'))["Password"];
					}
				var encodedData = window.btoa(UserName+':'+Password); // encode a string
				var config = {
					headers : {'Authorization' : 'Basic '+encodedData}
				   };
				//END OF ENCODE
		return service;

		// *******************************************************
 
		function getVersion(callback){
		 
			$http.get(url)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					result = data;
				 
					callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					//alert('no');
					console.log('ERROR (main):' +data+ status);
					callback(result);
				});
			
			  
			 
		}


			function getDataVersion(callback){
		    var urlData = window.localStorage.getItem('domain')+'/api/KwtrgDB/getDataVersion' ;
			$http.get(urlData)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					result = data;
				 
					callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					//alert('no');
					console.log('ERROR (main):' +data+ status);
					callback(result);
				});
			
			  
			 
		}
		function getPopupAd(callback) {
			var country = window.localStorage.getItem('Country_ID') === null ? 1 : window.localStorage.getItem('Country_ID');
		
			var urlData = window.localStorage.getItem('domain') + '/api/KwtrgDB/GetAd/0/' + country;      
			$http.get(urlData)
			  .success(function (data, status, headers, config) {
				// this callback will be called asynchronously
				// when the response is available
				result = data;
				//console.log(data);
				callback(result);
			  })
			  .error(function (data, status, headers, config) {
				// called asynchronously if an error occurs
				// or server returns response with an error status.
				//alert('no');
				console.log('ERROR (main):' + data + status);
				callback(result);
			  });
		  }
	  
	  
		  function getSliderAd(callback) {
	  
		  //console.log("SLIDER");
		  var country = window.localStorage.getItem('Country_ID') === null ? 1 : window.localStorage.getItem('Country_ID');
			  var urlData = window.localStorage.getItem('domain') + '/api/KwtrgDB/GetAd/1/' + country;
			  //var urlData = 'http://localhost:61403/api/KwtrgDB/GetAd/0/' +  window.localStorage.getItem('Country_ID');
			  $http.get(urlData,config)
				.success(function (data, status, headers, config) {
				  // this callback will be called asynchronously
				  // when the response is available
				  result = data;
				  //console.log(data);
				  callback(result);
				})
				.error(function (data, status, headers, config) {
				  // called asynchronously if an error occurs
				  // or server returns response with an error status.
				  //alert('no');
				  console.log('ERROR (main):' + data + status);
				  callback(result);
				});
			}



 
		
		

		 
	}
})();