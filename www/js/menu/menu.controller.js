(function() {
	'use strict';

	angular
		.module('kwtrg.menu')
		.controller('MenuController', MenuController);

	MenuController.$inject = ['$ionicPopup','NotifyService','$scope','$state'];

	/* @ngInject */
	function MenuController($ionicPopup,NotifyService,$scope,$state) {


		var vm = angular.extend(this, {
			Name: "Welcome",
			Tour:Tour,
			Count:0,
			IsRegister:0,
			navigateAdd:navigateAdd,
			navigateMyadd:navigateMyadd
          	 
		});

		$scope.$on('$ionicView.beforeEnter', function (event, viewData) {

			NotifyService.count(function(data){
		 
				vm.Count = data;
			 
			}); 

			if (window.localStorage.getItem("user") ===null)
			{
				vm.IsRegister=0;
			}
			else
			{
				vm.IsRegister=1;
			}

		});


		function navigateAdd() {
		
			if (window.localStorage.getItem("user") ===null)
			{
			 
				$state.go('app.tabs.AddBo3qar2');
			}
			else
			{ 	 
				$state.go('app.tabs.AddItem2');
			}
		}

		function navigateMyadd() {
		
			if (window.localStorage.getItem("user") ===null)
			{
			 
				$state.go('app.tabs.bo3qars2');
			}
			else
			{ 	 
				$state.go('app.tabs.myAds2');
			}
		}
        
	  //alert (userInfo["IsActive"]);
	  


		// $scope.$on('$ionicView.loaded', function (event, viewData) {
			 
			 
		
			 
		//   }); 
      
      
		  (function activate() {
			var  userInfo = JSON.parse( window.localStorage.getItem('user'));
			  if (window.localStorage.getItem("user") ===null)
				{
                 vm.Name= "Welcome";
				}
				else
				{
					vm.Name= userInfo["FirstName"];
				}

			

		  })();

		  function Tour ()
		  {
			   window.localStorage.setItem('mainTour',1);
                window.localStorage.setItem('ItemsTour',1);
				  window.localStorage.setItem('ItemsDetail',1);
				   window.localStorage.setItem('GallaryTour',1);
				    window.localStorage.setItem('myAds',1);
			  		var alertPopup = $ionicPopup.alert({ 
												title: "ارشادات تعليمية" ,    
												template: "{{ 'HelpOn' | translate }}"
												});
												 
									          //  alert(ionic.Platform.platform());
                                       
		  }
	  

	}
})();