(function() {
	'use strict';

	angular
		.module('kwtrg.myAds')
		.controller('myAdsArticlesController', myAdsArticlesController);

	myAdsArticlesController.$inject = ['$rootScope','$scope', '$state','$ionicPopup', 'myAdsService',  'ionicMaterialMotion','$stateParams','$filter','$ionicLoading'];



 




	/* @ngInject */
	function myAdsArticlesController($rootScope,$scope, $state,$ionicPopup, myAdsService,ionicMaterialMotion,$stateParams,$filter,$ionicLoading) {
		var vm = angular.extend(this, {
			articles: [],
			navigate: navigate,
			renew:renew,
			deleteAds:deleteAds,
			EditAds:EditAds,
			Country: [],
			sortBy: 'Date',
			selectedCategory: '1',
			doRefresh: doRefresh,
			CountryID: 1, 
			TypeId:parseInt($stateParams.TypeId),
			serviceID:parseInt($stateParams.serviceId),
			UserID:JSON.parse( window.localStorage.getItem('user'))["User_ID"],
		    showConfirm:showConfirm,	 
			 showAccountsWalkthrough:false,
			 	submitSearch: submitSearch,
		 
			City: [],
			 
			Region: [],
			Feature:[],
			Types:[],
			somePlaceholder:$filter('translate')('Search2'),

		});

  
       //TOUR
				var mainTour=window.localStorage.getItem('myAds');
				
				if (mainTour==1  || mainTour === null || !angular.isDefined(mainTour) )
				{
					setTimeout(function(){ 
								vm.showAccountsWalkthrough = true;
									window.localStorage.setItem('myAds',0);
						}, 500);		
				}
		//ENDOFTOUR



	  	$scope.$on('$ionicView.loaded', function (event, viewData) {	
         if (window.localStorage.getItem("user") !==null)
												{
													$ionicLoading.show({
													
																	duration:30000,	 
																	noBackdrop: true,
																	template: ' <div class="spinner2">  <ion-spinner  icon="ripple" class="spinner-assertive"></ion-spinner>  </div> </ion-content>'
																	});
																
															myAdsService.all(function(data){
															vm.articles = data;	 
															$ionicLoading.hide({	});
														}, TypeId,serviceID);
												}

		   }); 
		

	
		$scope.$on('$ionicView.beforeEnter', function (event, viewData) {
							viewData.enableBack = true;
							userInfo = JSON.parse( window.localStorage.getItem('user'));
												if (window.localStorage.getItem("user") ===null)
												{
														// var alertPopup = $ionicPopup.alert({
														// 		title: 'لا يمكنك المتابعة ',
														// 		template: "{{ 'loginRequired' | translate }}"  
														// 		});
															
														// $state.go('login');
														$state.go('app.tabs.bo3qars2');
													
												}
						}); 


	 
		// ********************************************************************

        var TypeId = parseInt($stateParams.TypeId);
		var serviceID = parseInt($stateParams.serviceId);
	    var userInfo=-1;
		var searchText ='';
		  var CountryID =parseInt(window.localStorage.getItem('Country_ID'));
      

			(function activate() {
				 
			    
  	         	 
			
		})();


		function navigate(articleId) {
			$state.go('app.article', { articleId: articleId });
		}


       function renew(articleId) {
		   
		   	myAdsService.renew(function(data){
		        var alertPopup = $ionicPopup.alert({
                title: 'Renew Status !',
                template: data
                 });
 
		}, articleId);
			//$state.go('app.article', { articleId: articleId });
		}
		
       
	      function deleteAds(articleId) {
		   
		   	myAdsService.deleteAds(function(data){
		        var alertPopup = $ionicPopup.alert({
                title: 'delete Status !',
                template: data
                 });
 
		}, articleId);
			//$state.go('app.article', { articleId: articleId });
		}

		    function showConfirm(articleId) {
			var confirmPopup = $ionicPopup.confirm({
					title: 'رسالة تاكيد  ',
				template: "{{ 'SureMsg' | translate }}"   
			});

			confirmPopup.then(function(res) {
				if(res) {
				console.log('You are sure-->'+articleId);
				deleteAds(articleId);
				} else {
				console.log('You are not sure');
				}
			});
			};


		function EditAds(articleId) {
		 
		   
		//    	myAdsService.deleteAds(function(data){
		//         var alertPopup = $ionicPopup.alert({
        //         title: 'delete Status !',
        //         template: data
        //          });
 
		// }, articleId);
         $state.go('app.tabs.EditItem' , { ItemId: articleId });
		
		}
		

		 
		function doRefresh() {
		setTimeout(
			 		
				myAdsService.all(function(data){
				vm.articles = data;
				$scope.$broadcast('scroll.refreshComplete');
			}, TypeId,serviceID)

		, 30000)
		
		}

        
		 	function submitSearch() {
				 

			   cordova.plugins.Keyboard.close();
                cordova.plugins.Keyboard.close();
  	         	$ionicLoading.show({
			    duration:30000,	 
				noBackdrop: true,
				template: ' <div class="spinner2">  <ion-spinner  icon="ripple" class="spinner-assertive"></ion-spinner>  </div> </ion-content>'

             	});
                 
				myAdsService.GetResult(function(data){
				    
							vm.articles = data;
							$ionicLoading.hide({	});
						},searchText, serviceID,TypeId,CountryID); 

				 $ionicScrollDelegate.scrollTop();
				 
				}
  



       
		 $scope.selectedCountry= function($item) {
			
		  $state.go('app.article', { articleId: $item.originalObject.value });

			 
			 
			 };
	   
     $scope.inputChanged= function($item) {
	
		 searchText= $item ;	 };
		 
	}
})();