(function() {
	'use strict';

	var propertiesView = {
		templateUrl: 'js/myAds/articles.html',
		controller: 'myAdsArticlesController as vm'
	};

	angular
		.module('kwtrg.myAds', [
			'ionic',
			'angucomplete-alt'
			 
		])
		.config(function($stateProvider) {
			$stateProvider
				.state('app.tabs.myAds', {
		
					url: '/myAds/:TypeId/:serviceId',
				views: { 'tab-Fav': propertiesView },
			 
				 
				})

				.state('app.tabs.myAds2', {
		
					url: '/myAds/:TypeId/:serviceId',
				views: { 'tab-Fav': propertiesView },
			 
				 
				})

				.state('app.AdsDetails', {
				 	 cache: false,
					url: '/articles/:articleId',
					views: {
						'menuContent': {
							templateUrl: 'js/myAds/article.html',
							controller: 'ArticleController2 as vm'
						}
					}
				});
		});
})();