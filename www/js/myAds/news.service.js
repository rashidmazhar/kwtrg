(function() {
	'use strict';

	angular
		.module('kwtrg.myAds')
		.factory('myAdsService', myAdsService);

	myAdsService.$inject = ['$http', '$q'];

	/* @ngInject */
	function myAdsService($http, $q) {
	//var url ='http://skounis.s3.amazonaws.com/mobile-apps/barebone-glossy/news.json';
		
		 
		var result = [];
        
		var service = {
			all: all,
			get: get,
			renew: renew,
			deleteAds:deleteAds,
			allResults: allResults,
			allCountry:allCountry,
			allCity:allCity,
			allRegion:allRegion,
			allTypes:allTypes,
			getFeature:getFeature,
			allSort:allSort,
			GetResult:GetResult
		};
		    //ENCODE USER
			var	UserName ="";
			var Password="";
				if (window.localStorage.getItem("user") !==null)
				{
					 UserName = JSON.parse( window.localStorage.getItem('user'))["UserName"];
					 Password = JSON.parse( window.localStorage.getItem('user'))["Password"];
				}
			var encodedData = window.btoa(UserName+':'+Password); // encode a string
			var config = {
				headers : {'Authorization' : 'Basic '+encodedData}
			   };
			//END OF ENCODE
		return service;

		// *******************************************************

		// http://stackoverflow.com/questions/17533888/s3-access-control-allow-origin-header
		function all(callback , TypeId, serviceID){
			var userinfo  = JSON.parse( window.localStorage.getItem('user'));
         var url = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetItemsmyAds?UserID='+userinfo["User_ID"]+'&Lang='+window.localStorage.getItem('lang'); 
			$http.get(url,config)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					result = data;
					
					callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					alert('no');
					console.log('ERROR (News):' + status);
					callback(result);
				});
		}


				function renew(callback , articleId){
			var userinfo  = JSON.parse( window.localStorage.getItem('user'));
         var url = window.localStorage.getItem('domain')+'/api/KwtrgDB/RenewItem?item_Id='+articleId+'&Lang='+window.localStorage.getItem('lang'); 
			$http.get(url,config)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					result = data;
					
					callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					alert('no');
					console.log('ERROR (News):' + status);
					callback(result);
				});
		}


function deleteAds(callback , articleId){
			var userinfo  = JSON.parse( window.localStorage.getItem('user'));
         var url = window.localStorage.getItem('domain')+'/api/KwtrgDB/DeleteItemApp?id='+articleId+'&Lang='+window.localStorage.getItem('lang'); 
			$http.get(url,config)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					result = data;
					
					callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					alert('no');
					console.log('ERROR (News):' + status);
					callback(result);
				});
		}
        

	function allSort(callback , Item ){
         var url = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetItemsFilter';
		// ?countryID=1&TypeId='+TypeId+'&Lang='+window.localStorage.getItem('lang')+'&serviceID='+serviceID+'&SelectedCountry='+selectedcountry+'&City_ID='+selectedCity+'&Region_ID='+selectedRegion+'&SelectedType='+selectedTypeProperty+'&sortOrder='+sortBy+'&Feature='+JSON.stringify(Feature); 
		 
			  $http({
                url: url,
                method: 'POST',
				dataType: "json",
                data:  Item,
				contentType: "application/json",
              //  headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
				 	result = data;
				 
					callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					alert('no');
					console.log('ERROR (News):' + status);
					callback(result);
				});
		}


		function allResults(callback , TypeId, serviceID, SearchString){
         var url = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetItemsSearchString?countryID=1&TypeId='+TypeId+'&Lang='+window.localStorage.getItem('lang')+'&serviceID='+serviceID+'&SearchString='+SearchString; 
			$http.get(url)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					//alert(result);
					result = data;
					
					callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					alert('no');
					console.log('ERROR (News):' + status);
					callback(result);
				});
		}
         



        
		function allCountry(callback){
		 var urlCountry = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetCountriesApp?Lang='+window.localStorage.getItem('lang');
			$http.get(urlCountry)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					result = data;
			 
				    
					callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					alert('Error : Try Again');
					console.log('ERROR (gallary):' + status);
					callback(result);
				});
			
			  
			 
		}


	 function allCity(callback, id){
		
		 var urlCity = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetCitiesByCountriesApp?Lang='+window.localStorage.getItem('lang')+"&id="+id;
			$http.get(urlCity)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					result = data;
				 
					callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					alert('Error : Try Again');
					console.log('ERROR (gallary):' + status);
					callback(result);
				});	 
		}
     

	       function allRegion(callback, id){
		 var urlCity = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetRegionsByCitiesApp?Lang='+window.localStorage.getItem('lang')+"&id="+id;
			$http.get(urlCity)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					result = data;
				    
					callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					alert('Error : Try Again');
					console.log('ERROR (gallary):' + status);
					callback(result);
				});
			
			  
			 
		}  
	  function allTypes(callback, id){
		 var urlType = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetTypeByCountryIDApp?Lang='+window.localStorage.getItem('lang')+"&id="+id;
			$http.get(urlType)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					result = data;
				    
					callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					alert('Error : Try Again');
					console.log('ERROR (gallary):' + status);
					callback(result);
				});
			
			  
			 
		} 
			

		function getFeature(callback ,TypeID,CountryID) {
		 
		 var urlFeature = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetTypeFieldsApp?Lang='+window.localStorage.getItem('lang')+"&Type="+TypeID+"&CountryId="+CountryID;
		 
		$http.get(urlFeature)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					result = data;
				  
					callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					alert('Error : Try Again');
					console.log('ERROR (gallary):' + status);
					callback(result);
				});
	}

		function get(callback,articleId) {
			// we take an article from cache but we can request ir from the server
			var urlItem=  window.localStorage.getItem('domain')+'/api/KwtrgDB/GetItemsFull?itemID='+articleId+'&Lang='+ window.localStorage.getItem('lang');
		        $http.get(urlItem)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					result = data;
					
					
					callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					alert('no');
					console.log('ERROR (News):' + status);
					callback(result);
				});
		}

		function GetResult (callback,term,ServiceId,TypeID,CounrtyID)
				{
					var userinfo  = JSON.parse( window.localStorage.getItem('user'));
					var urlSearch=  window.localStorage.getItem('domain')+'/api/KwtrgDB/GetItemsMyAds?UserID='+userinfo["User_ID"]+'&ServiceId='+ServiceId+'&TypeID='+TypeID+'&countryID='+CounrtyID+'&term='+term+'&Lang='+window.localStorage.getItem('lang');
				 
						$http.get(urlSearch,config)
						.success(function(data, status, headers, config) {
							// this callback will be called asynchronously
							// when the response is available
							result = data;
						
							callback(result);
						})
						.error(function(data, status, headers, config) {
							// called asynchronously if an error occurs
							// or server returns response with an error status.
							//alert('no');
							console.log('ERROR (Search-GetResult):'+data + status);
							callback(result);
						});
				}

	}
})();