(function() {
  "use strict";

  angular
    .module("kwtrg.news")
    .controller("ArticleController", ArticleController);

  ArticleController.$inject = [
    "$ionicModal",
    "$stateParams",
    "newsService",
    "$scope",
    "$ionicSlideBoxDelegate",
    "$ionicLoading",
    "$ionicConfig",
    "$ionicNavBarDelegate",
    "$timeout"
  ];

  /* @ngInject */
  function ArticleController(
    $ionicModal,
    $stateParams,
    newsService,
    $scope,
    $ionicSlideBoxDelegate,
    $ionicLoading,
    $ionicConfig,
    $ionicNavBarDelegate,
    $timeout,
    shareService
  ) {
    var vm = angular.extend(this, {
      article: [],
      showAccountsWalkthrough: false
    });

    /* SLIDER ***/

    $ionicModal
      .fromTemplateUrl("image-modal.html", {
        scope: $scope,
        animation: "slide-in-up"
      })
      .then(function(modal) {
        $scope.modal = modal;
      });

    $scope.openModal = function() {
      $ionicSlideBoxDelegate.slide(0);
      $scope.modal.show();
    };

    $scope.closeModal = function() {
      $scope.modal.hide();
    };

    // Cleanup the modal when we're done with it!
    $scope.$on("$destroy", function() {
      $scope.modal.remove();
    });
    // Execute action on hide modal
    $scope.$on("modal.hide", function() {
      // Execute action
    });
    // Execute action on remove modal
    $scope.$on("modal.removed", function() {
      // Execute action
    });
    $scope.$on("modal.shown", function() {
      console.log("Modal is shown!");
    });

    // Call this functions if you need to manually control the slides
    $scope.next = function() {
      $ionicSlideBoxDelegate.next();
    };

    $scope.previous = function() {
      $ionicSlideBoxDelegate.previous();
    };

    $scope.goToSlide = function(index) {
      $scope.modal.show();
      $ionicSlideBoxDelegate.slide(index);
    };

    // Called each time the slide changes
    $scope.slideChanged = function(index) {
      $scope.slideIndex = index;
    };

    // **********************************************************	END OF SLIDER *********************/

    //share
    $scope.OtherShare = function() {
      if (window.plugins.socialsharing) {
        var lang = window.localStorage.getItem("lang");
        var message = "";
        if (lang === "en") {
          message =
            "I am looking for more information regards :\n \n Ads title : " +
            vm.article[0].Title +
            "\n \n Ads url : http://bo3qar.com/Share?id=" +
            parseInt($stateParams.articleId);
        } else {
          message =
            " یرجى التكرم بتزویدنا بتفاصیل العقار التالي :\n \n عنوان الأعلان :" +
            vm.article[0].Title +
            "\n \n الرابط : http://bo3qar.com/Share?id=" +
            parseInt($stateParams.articleId);
        }
        window.plugins.socialsharing.shareViaWhatsAppToPhone(
          vm.article[0].Phone,
          message,
          null,
          null,
          function(e) {
            //do something
            console.log(e);
          },
          function(e) {
            console.log(e);
          }
        );
      }

      //   window.plugins.socialsharing.share(
      //     "Bo3qar - " + vm.article[0].Title,
      //     null,
      //     null,
      //     "http://bo3qar.com/Share?id=" + parseInt($stateParams.articleId)
      //   );
    };
    //TOUR
    var mainTour = window.localStorage.getItem("ItemsDetail");

    if (mainTour == 1 || mainTour === null || !angular.isDefined(mainTour)) {
      setTimeout(function() {
        vm.showAccountsWalkthrough = true;
        window.localStorage.setItem("ItemsDetail", 0);
      }, 500);
    }

    //ENDOFTOUR

    $scope.$on("$ionicView.beforeEnter", function(event, viewData) {
      $ionicNavBarDelegate.showBackButton(true);
      viewData.enableBack = true;
    });

    $scope.$on("$ionicView.loaded", function(event, viewData) {
      viewData.enableBack = true;

      $ionicLoading.show({
        noBackdrop: true,
        template:
          ' <div class="spinner2">  <ion-spinner  icon="ripple" class="spinner-assertive"></ion-spinner>  </div> </ion-content>'
      });
    });

    // ********************************************************************
    (function activate() {
      var articleId = parseInt($stateParams.articleId);
      var IsBo3qar = parseInt($stateParams.IsBo3qar);
      console.log(articleId);
      console.log(IsBo3qar);

      newsService.get(function(data) {
        vm.article = data;
        $ionicSlideBoxDelegate.update();
        $ionicSlideBoxDelegate.loop(true);
        $ionicLoading.hide({});
      }, articleId);

      if (IsBo3qar === 1 || IsBo3qar === "1") {
        newsService.UpdateOpneToke(function(data) {}, articleId);
      }
    })();

    $scope.CallNumber = function() {
      var number = vm.article[0].Phone;
      if (window.plugins.CallNumber) {
        window.plugins.CallNumber.callNumber(
          function() {
            newsService.CallCounter(null, parseInt($stateParams.articleId));
            //success logic goes here
          },
          function() {
            //error logic goes here
          },
          number
        );
      }
    };

    // newsService.get(articleId)
    // 	.then(function(article) {
    // 		vm.article = article;
    // 	});
  }
})();
