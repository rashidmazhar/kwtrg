(function() {
	'use strict';

	var propertiesView = {
		templateUrl: 'js/news/articles.html',
		controller: 'ArticlesController as vm'
	};

	angular
		.module('kwtrg.news', [
			'ionic',
			'angucomplete-alt'
			 
		])
		.config(function($stateProvider) {
			$stateProvider
				.state('app.tabs.articles', {
					url: '/articles/:TypeId/:serviceId',
				views: { 'tab-home': propertiesView },
				resolve: {
						filterModal: function($ionicModal, $rootScope) {
							return $ionicModal.fromTemplateUrl('js/news/item-filter.html', {
								scope: $rootScope,
								animation: 'slide-in-up'
							});
						}
					}
				 
				})
				.state('app.article', {
						 cache: false,
						 params: {articleId: 0 , IsBo3qar:0},
					url: '/articles/:articleId/:IsBo3qar',
					views: {
						'menuContent': {
							templateUrl: 'js/news/article.html',
							controller: 'ArticleController as vm'
						}
					}
				});
		});
})();