(function() {
	'use strict';

	angular
		.module('kwtrg.Notify')
		.controller('NotifyNotifiesController', NotifyNotifiesController);

	NotifyNotifiesController.$inject = ['$rootScope','$scope', '$state','$ionicPopup', 'NotifyService',  'ionicMaterialMotion','$stateParams','$filter','$ionicLoading'];



 




	/* @ngInject */
	function NotifyNotifiesController($rootScope,$scope, $state,$ionicPopup, NotifyService,ionicMaterialMotion,$stateParams,$filter,$ionicLoading) {
		var vm = angular.extend(this, {
			notifies: [],
			navigate: navigate,
		 
			Country: [],
			sortBy: 'Date',
			selectedCategory: '1',
			doRefresh: doRefresh,
			CountryID: 1, 
			TypeId:parseInt($stateParams.TypeId),
			serviceID:parseInt($stateParams.serviceId),
			UserID:1,
		 
		 
			City: [],
			 
			Region: [],
			Feature:[],
			Types:[],
			somePlaceholder:$filter('translate')('Search2'),

		});

  
		// $scope.backButton = function() {
		// 	console.log('back');
		// 	$ionicNavBarDelegate.back();
		//   };

// 		  $rootScope.$ionicGoBack = function() {
// 	alert ("Retung");
// 	// implement custom behaviour here
// 	this.nav.popToRoot();
	
// };



	  
		$scope.$on('$ionicView.loaded', function (event, viewData) {	
			viewData.enableBack = true;
													   $ionicLoading.show({
													   
																	   duration:30000,	 
																	   noBackdrop: true,
																	   template: ' <div class="spinner2">  <ion-spinner  icon="ripple" class="spinner-assertive"></ion-spinner>  </div> </ion-content>'
																	   });
																   
																	   NotifyService.all(function(data){
															   vm.notifies = data;	 
															   $ionicLoading.hide({	});
														   }, 1,1);
												   
   
			  }); 
		

	
		$scope.$on('$ionicView.beforeEnter', function (event, viewData) {
							viewData.enableBack = true;
							 
						}); 


	 
		// ********************************************************************

        var TypeId = parseInt($stateParams.TypeId);
		var serviceID = parseInt($stateParams.serviceId);
	    var userInfo=-1;
		var searchText ='';
		  var CountryID =parseInt(window.localStorage.getItem('Country_ID'));
      

			(function activate() {
				 
			    
  	         	 
			
		})();


		function navigate(notifyId) {
			 
			$state.go('app.notify', { notifyId: notifyId });
			console.log("Call Notify Page");
		}
 

 
		

		 
		function doRefresh() {
		setTimeout(
			 		
				NotifyService.all(function(data){
				vm.notifies = data;
				$scope.$broadcast('scroll.refreshComplete');
			}, TypeId,serviceID)

		, 30000)
		
		}

 
		 
	}//Controller
})();