(function() {
	'use strict';

	var propertiesView = {
		templateUrl: 'js/notify/notifies.html',
		controller: 'NotifyNotifiesController as vm'
	};


	angular
		.module('kwtrg.Notify', [
			'ionic',
			'angucomplete-alt'
			 
		])
		.config(function($stateProvider) {
			$stateProvider
	 

				.state('app.tabs.notifies', {
					url: '/notifies', 	views: { 'tab-setting': propertiesView }
					
				 
				})
			

				.state('app.notify', {
				 	 cache: false,
					url: '/notify/:notifyId',
					views: {
						'menuContent': {
							templateUrl: 'js/notify/notify.html',
							controller: 'NotifyController2 as vm'
						}
					}
				});
		});
})();