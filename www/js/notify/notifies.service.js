(function() {
	'use strict';

	angular
		.module('kwtrg.Notify')
		.factory('NotifyService', NotifyService);

	NotifyService.$inject = ['$http', '$q'];

	/* @ngInject */
	function NotifyService($http, $q) {
	//var url ='http://skounis.s3.amazonaws.com/mobile-apps/barebone-glossy/news.json';
		
		 
		var result = [];
        
		var service = {
			all: all,
			get: get,
			count: count
		
		};
		return service;

		// *******************************************************

		// http://stackoverflow.com/questions/17533888/s3-access-control-allow-origin-header
		function all(callback ){
			var token  =  window.localStorage.getItem('registrationId');
			
		 
			if (token === undefined || token ==="" || token===null || token==="null")
			{
				token="fx7_ym7VDsY:APA91bF5z1kK0cVgcmzCk1Uf2b_HPQKAeDmN4-hgx2qNz2CWoyY5UiOmvZWQCoA6QTBBJeORpB1AyyixDCMYys70_xhpwQ0d51GDkgtmuMqKMF5oRKoNMtWnLADamSWpfhw8H6mcbD_i";
			}
			// alert(token);
		 var url = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetItemsNotify?_token='+token; 
		 console.log(url);
			$http.get(url)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					result = data;
					 
					
					callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					alert('no');
					console.log('ERROR (News):' + status);
					callback(result);
				});
		}


	 function count(callback ){
		var token  =  window.localStorage.getItem('registrationId');

			if (token === undefined || token ==="" || token===null || token==="null")
			{
				token="fx7_ym7VDsY:APA91bF5z1kK0cVgcmzCk1Uf2b_HPQKAeDmN4-hgx2qNz2CWoyY5UiOmvZWQCoA6QTBBJeORpB1AyyixDCMYys70_xhpwQ0d51GDkgtmuMqKMF5oRKoNMtWnLADamSWpfhw8H6mcbD_i";
			}
		  
         var url = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetItemsNotifyCount?_token='+token; 
			$http.get(url)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					result = data;
					//alert(data);
					callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					alert('no');
					console.log('ERROR (Count):' + status);
					callback(result);
				});
		}


			
 
		function get(callback,_notifyId) {
			var token  =  window.localStorage.getItem('registrationId');

			if (token === undefined || token ==="" || token===null || token==="null")
			{
				token="fx7_ym7VDsY:APA91bF5z1kK0cVgcmzCk1Uf2b_HPQKAeDmN4-hgx2qNz2CWoyY5UiOmvZWQCoA6QTBBJeORpB1AyyixDCMYys70_xhpwQ0d51GDkgtmuMqKMF5oRKoNMtWnLADamSWpfhw8H6mcbD_i";
			}
		  

			var	UserID ="";
			if (window.localStorage.getItem("user") !==null)
			{
			 	UserID = JSON.parse( window.localStorage.getItem('user'))["User_ID"];
			}
		 
			 
 var url = window.localStorage.getItem('domain')+'/api/KwtrgDB/getItemNotify?_notifyId='+_notifyId+'&_token='+token+'&_userId='+UserID; 
 console.log(url);
	$http.get(url)
		.success(function(data, status, headers, config) {
			// this callback will be called asynchronously
			// when the response is available
			result = data;
		 
			callback(result);
		})
		.error(function(data, status, headers, config) {
			// called asynchronously if an error occurs
			// or server returns response with an error status.
			alert('no');
			console.log('ERROR (News):' + status);
			callback(result);
		});
		}

 

	}
})();