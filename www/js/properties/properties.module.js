(function() {
	'use strict';

	var propertyResolve = {
		property: function($stateParams, propertiesService) {
			return propertiesService.getProperty($stateParams.propertyId);
		}
	};

	var propertyView = {
		templateUrl: 'js/properties/property.html',
		controller: 'PropertyController as vm',
		resolve: propertyResolve
	};

	var propertiesView = {
		templateUrl: 'js/properties/properties-list.html',
		controller: 'PropertiesListController as vm'
	};

	angular
		.module('kwtrg.properties', [
			'ionic',
			'ion-autocomplete'
		])
		.config(function($stateProvider) {
			$stateProvider
				.state('app.properties', {
					url: '/properties',
					abstract: true,
					views: {
						'menuContent': {
							templateUrl: 'js/properties/properties.html'
						}
					},
					resolve: {
						filterModal: function($ionicModal, $rootScope) {
							return $ionicModal.fromTemplateUrl('js/properties/properties-filter.html', {
								scope: $rootScope,
								animation: 'slide-in-up'
							});
						}
					}
				})
				.state('app.properties.rent', {
					url: '/rent',
					views: { 'tab-rent': propertiesView }
				})
				.state('app.properties.sale', {
					url: '/sale',
					views: { 'tab-sale': propertiesView }
				})
				.state('app.properties.property-rent', {
					url: '/rent/:propertyId',
					views: { 'tab-rent': propertyView }
				})
				.state('app.properties.property-sale', {
					url: '/sale/:propertyId',
					views: { 'tab-sale': propertyView }
				})
				.state('app.property', {
					url: '/properties/:propertyId',
					views: { 'menuContent': propertyView }
				});
			;
		});
})();