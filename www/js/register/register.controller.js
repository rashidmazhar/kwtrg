(function() {
	'use strict';

	angular
		.module('kwtrg.register')
		.controller('registerController', registerController);

	registerController.$inject = [
    '$state',
		 '$stateParams',
		 '$scope',
		'$timeout',
	 'RegisterService',
	'$ionicPopup','filterFilter','$ionicLoading','$ionicActionSheet','$filter','$cordovaCamera'
	];

	/* @ngInject */
	function registerController($state,$stateParams,$scope, $timeout,RegisterService, $ionicPopup,filterFilter,$ionicLoading,$ionicActionSheet,$filter,$cordovaCamera) {
        var allcountry = JSON.parse( window.localStorage.getItem('allCountry'));
		var allCity = JSON.parse( window.localStorage.getItem('allCity'));
		var allRegions = JSON.parse( window.localStorage.getItem('allRegion'));
		var Lang=window.localStorage.getItem('lang');
	    var CountryID =parseInt(window.localStorage.getItem('Country_ID'));
        var remoteFiles = [];
        var id=0;
       

		var vm = angular.extend(this, {
			entries: [],
		   City: [],
		   Region:  [],
           
 
		});
      
       
		
	(function activate() {
         
      $scope.ShowPathy=[{id: 0, value:false}, {id: 1, value:false}, {id: 2, value:false}, {id: 3, value:false}]; 
      $scope.ImagePathy =[{id: 0, value:"Img/slider2.png"}, {id: 1, value:"Img/slider2.png"}, {id: 2, value:"Img/slider2.png"}, {id: 3, value:"Img/slider2.png"}];
     // id = Math.floor(Math.random()*9999);
      id = parseInt ( Math.floor(Math.random()*9999))+   parseInt ($filter('date')(new Date(), 'dd')) + parseInt ( $filter('date')(new Date(), 'MM')) + parseInt (  $filter('date')(new Date(), 'HH') ) +  ($filter('date')(new Date(), 'mm')) +   ( $filter('date')(new Date(), 'ss')) ;
	  vm.entries =allcountry;
      $scope.selectedcountry= CountryID;
      vm.City =	filterFilter(allCity, {Country_ID:CountryID},true);
	 
		})();

		// **********************************************************

	 


	       $scope.change = function(selectedItem) {

         vm.City =	filterFilter(allCity, {Country_ID:parseInt(selectedItem)},true);
		     vm.Types=  filterFilter(allTypes, {Country_ID:parseInt(selectedItem)},true);   

       };


     $scope.changeCity = function(selectedCity) {
            	vm.Region =	filterFilter(allRegions, {City_ID:parseInt(selectedCity)},true); 
             };       
           


          $scope.submit = function(selectedcountry,selectedCity,selectedRegion,Mobile,FirstName,LastName,Email,NumCode,password) {
            cordova.plugins.Keyboard.close();

			// alert (selectedcountry+"City-->"+selectedCity+"region-->"+selectedRegion+"mobile-->"+Mobile +"first-->"+FirstName +"last-->"+ LastName +"email-->"+ Email +"agg-->"+ChkAgrement +"vode-->"+ NumCode+"password-->"+ password);
	 $ionicLoading.show({
              duration:30000,	 
              noBackdrop: true,
              template: ' <div class="spinner2">  <ion-spinner  icon="ripple" class="spinner-assertive"></ion-spinner>  </div> </ion-content>' });
           
            var User = 
                     {
                       "selectedcountry":selectedcountry,
                         "selectedCity" :selectedCity,
                         "selectedRegion": selectedRegion ,
                         "Mobile":Mobile,
                         "FirstName":FirstName,
                         "LastName":LastName,
                         "Email":Email,
                         "ChkAgrement":1,
                         "NumCode":NumCode,
                         "password":password,
                         "Lang":Lang,
                         "Session":id
                     

                     };
                 RegisterService.InserUser (function(data){
                      $ionicLoading.hide({	});
                     if (data === "1" ) {
                        var alertPopup = $ionicPopup.alert({
                            title: 'مستخدم جديد',
                            template: "{{ 'NewUser' | translate }}"  
                            });
                          $state.go('login');
                     }
                     
                     else{
                           var alertPopup = $ionicPopup.alert({
                            title: 'مستخدم جديد',
                            template: data  
                            });}
                      },User)   ;
            
             }




              $scope.show = function(duration) {
                  
                   
                              console.log('working action');
                            // Show the action sheet
                            var hideSheet = $ionicActionSheet.show({
                                buttons: [
                                { text: '<i class="icon-left ion-camera"></i>    ' + $filter('translate')('Camera')},
                                { text: '<i class=" icon-left ion-images "></i>   '+$filter('translate')('Import')} 
                                ],
                                destructiveText:  $filter('translate')('Delete'),
                                destructiveButtonClicked: function() {
                                      
                                     $scope.ShowPathy[0].value=false;
                                     $scope.ImagePathy[0].value= "Img/slider2.png";
                                    return true; //Close the model?
                                },
                                titleText: $filter('translate')('AddCompanyLogo'),  
                                cancelText: 'Cancel',
                                cancel: function() {
                                       
                                    // add cancel code..
                                    },
                                buttonClicked: function(index) {
                                     if(index === 0){ // Manual Button   
                                    
                         
                           
                                           $scope.takePhoto(index);
                                        }
                                        else if(index === 1){
                                       
                                            $scope.selImages(index-1);
                                        }
                                return true;
                                }
                            });

                            // For example's sake, hide the sheet after two seconds
                            $timeout(function() {
                                hideSheet();
                            }, duration);

                            };//END OF SHOW


//ACTIONSHEET REAL STATE

 $scope.showRE = function(duration) {
    
                              console.log('working action re');
                            // Show the action sheet
                            var hideSheet = $ionicActionSheet.show({
                                buttons: [
                                { text: '<i class="icon-left ion-camera"></i>    ' + $filter('translate')('Camera')},
                                { text: '<i class=" icon-left ion-images "></i>   '+$filter('translate')('Import')} 
                                ],
                                destructiveText:  $filter('translate')('Delete'),
                                destructiveButtonClicked: function() {
                                    $scope.ShowPathy[1].value=false;
                                     $scope.ImagePathy[1].value= "Img/slider2.png";
                                    return true; //Close the model?
                                },
                                titleText: $filter('translate')('AddRELicenseFull'),  
                                cancelText: 'Cancel',
                                cancel: function() {
                                       
                                    // add cancel code..
                                    },
                                buttonClicked: function(index) {
                                     if(index === 0){ // Manual Button   
                                     
                                       
                                            $scope.takePhoto(1);
                                        }
                                        else if(index === 1){
                                       
                                            $scope.selImages(1);
                                        }
                                return true;
                                }
                            });

                            // For example's sake, hide the sheet after two seconds
                            $timeout(function() {
                                hideSheet();
                            }, duration);

                            };//END OF SHOW



//ACTIONSHEET COMMERCIAL LICENSE

 $scope.showCL = function(duration) {
      
                              console.log('working action re');
                            // Show the action sheet
                            var hideSheet = $ionicActionSheet.show({
                                buttons: [
                                { text: '<i class="icon-left ion-camera"></i>    ' + $filter('translate')('Camera')},
                                { text: '<i class=" icon-left ion-images "></i>   '+$filter('translate')('Import')} 
                                ],
                                destructiveText:  $filter('translate')('Delete'),
                                destructiveButtonClicked: function() {
                                    $scope.ShowPathy[2].value=false;
                                     $scope.ImagePathy[2].value= "Img/slider2.png";
                                    return true; //Close the model?
                                },
                                titleText: $filter('translate')('AddCL'),  
                                cancelText: 'Cancel',
                                cancel: function() {
                                       
                                    // add cancel code..
                                    },
                                buttonClicked: function(index) {
                                     if(index === 0){ // Manual Button   
                                     
                        
                                            $scope.takePhoto(2);
                                        }
                                        else if(index === 1){
                                       
                                            $scope.selImages(2);
                                        }
                                return true;
                                }
                            });

                            // For example's sake, hide the sheet after two seconds
                            $timeout(function() {
                                hideSheet();
                            }, duration);

                            };//END OF SHOW



//ACTIONSHEET CIVIL ID

 $scope.showCID = function(duration) {
     
                              console.log('working action re');
                            // Show the action sheet
                            var hideSheet = $ionicActionSheet.show({
                                buttons: [
                                { text: '<i class="icon-left ion-camera"></i>    ' + $filter('translate')('Camera')},
                                { text: '<i class=" icon-left ion-images "></i>   '+$filter('translate')('Import')} 
                                ],
                                destructiveText:  $filter('translate')('Delete'),
                                destructiveButtonClicked: function() {
                                    $scope.ShowPathy[3].value=false;
                                     $scope.ImagePathy[3].value= "Img/slider2.png";
                                    return true; //Close the model?
                                },
                                titleText: $filter('translate')('AddCID'),  
                                cancelText: 'Cancel',
                                cancel: function() {
                                       
                                    // add cancel code..
                                    },
                                buttonClicked: function(index) {
                                     if(index === 0){ // Manual Button   
                                     
                        
                                            $scope.takePhoto(3);
                                        }
                                        else if(index === 1){
                                       
                                            $scope.selImages(3);
                                        }
                                return true;
                                }
                            });

                            // For example's sake, hide the sheet after two seconds
                            $timeout(function() {
                                hideSheet();
                            }, duration);

                            };//END OF SHOW





            /*CAMERA*/

$scope.takePhoto = function (newIndex) {
    
                  var options = {
                    targetWidth: 768,
                     targetHeight: 1024,
                   
                    destinationType: Camera.DestinationType.DATA_URL,
                    sourceType: Camera.PictureSourceType.CAMERA,
                    allowEdit: true,
                    encodingType: Camera.EncodingType.JPEG,
                   
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: false
                };
                   $('#I_'+ newIndex).addClass("clz_blur"); 
                    $cordovaCamera.getPicture(options).then(function (imageData) {
                        
                        var imgURI = "data:image/jpeg;base64," + imageData;
                        
                         var numberofImages = 0;
                        //  var newIndex = numberofImages;
                          console.log("img id= " + id);  
                          
                    console.log('Image URI: ' + imgURI);
                    $scope.ShowPathy[newIndex].value=true; 
                    $scope.ImagePathy[newIndex].value= imgURI;
                    $scope.show(0);
                         
                    console.log("pathy... "+   $scope.ImagePathy);  
                      console.log("Uploding... " );  
                      remoteFiles[numberofImages]=imgURI;
                      DoUploadCamera (newIndex);  
                        numberofImages=numberofImages+1; 

                    }, function (err) {
                      alert(err);
                      
                        // An error occured. Show a message to the user
                    });
                }//END OF CAMERA





              //SELECT IMAGE

           function hasReadPermission() {
                    window.imagePicker.hasReadPermission(
                    function(result) {
                        // if this is 'false' you probably want to call 'requestReadPermission' now
                        alert(result);
                    }
                    )
                }

                function requestReadPermission() {
                    // no callbacks required as this opens a popup which returns async
                    window.imagePicker.requestReadPermission();
                }

          
              $scope.selImages = function(id) {

                  window.imagePicker.getPictures(
                    function(results) {
                        remoteFiles=results;
                        $('#I_'+ newIndex).addClass("clz_blur");
                        for (var i = 0; i < 1; i++) { 
                              var newIndex = (i+id);
                            console.log('Image URI: ' + results[i]);
                            $scope.ShowPathy[newIndex].value=true; 
                             $scope.ImagePathy[newIndex].value= results[i];
                             $scope. show(0);
                        }
                         DoUpload(newIndex);
                    }, function (error) {
                        console.log('Error: ' + error);
                    }, {
                        maximumImagesCount: 1,
                        width: 768,
                        height :1024
                    }
                );


                            };
       //END OF SELECT




//Upload Camera Image

function DoUpload (Index){
 
    var newIndex = (Index);
  console.log("Uploading Call For -->"+Index+"-->"+ remoteFiles[Index] + "   ID--> "+id+" New Index --> " + newIndex);
    var FileName="Unknown";
    switch (Index) {
            case 0:
               FileName= id+"_CompanyLogoPhoto.jpeg";
               console.log("Current File-->"+FileName + "  Index-->"+Index );
                
                break;
            case 1:
                  FileName= id+"_RELicense.jpeg";
               console.log("Current File-->"+FileName + "  Index-->"+Index );
                break;

           case 2:
                  FileName= id+"_CLPhoto.jpeg";
               console.log("Current File-->"+FileName + "  Index-->"+Index );
                break;

           case 3:
                  FileName= id+"_CIDPhoto.jpeg";
               console.log("Current File-->"+FileName + "  Index-->"+Index );
                break;
            default:
             FileName= "_undefined.jpeg";
               console.log("Current File-->"+FileName + "  Index-->"+Index );
               break;

        }
  
    var uri = window.localStorage.getItem('domain')+'/api/KwtrgDB/SaveImage';
     $ionicLoading.show({			
								duration:30000,	 
								noBackdrop: true,
                                hideOnStateChange:true,
								 
								});
          
        var options = new FileUploadOptions();
        options.fileKey="Filedata";
        options.fileName=remoteFiles[0].substr(remoteFiles[0].lastIndexOf('/') + 1);
          options.chunkedMode= false;
        options.mimeType= "image/*";
          var params = new Object(); 
                    params.session1 = FileName ; 
                
                    options.params = params; 
        var headers={'headerParam':'headerValue'};

        options.headers = headers;

        var ft = new FileTransfer();
        ft.onprogress = function(progress) {
           console.log("Onprog-->"+newIndex+"-->"+ remoteFiles[Index]);
            var percent  = Math.round(progress.loaded / progress.total *360);
                  var loader = document.getElementById('b_'+ newIndex), 
                      border = document.getElementById('l_'+ newIndex),
                      a = 0, 
                    p = Math.PI; 
                  
                  (function draw() {
                    a = percent; 
                    a %= 360;
                    var r = a * p / 180, x = Math.sin(r) * 125, y = Math.cos(r) * -125, mid = a > 180 ? 1 : 0, 
                        anim = 'M 0 0 v -125 A 125 125 1 ' + mid + ' 1 ' + x + ' ' + y + ' z';
                  
                    loader.setAttribute('d', anim); 
                    border.setAttribute('d', anim);
                  
                  if (a !== 0){
                    draw();
                  } else if (a === 0){
                       $ionicLoading.hide({	});    
                    $('#I_'+ newIndex).removeClass("clz_blur"); 
                  }
                    
                  }()); 
        };
        ft.upload(remoteFiles[0], uri,function(r) {
                // Do what you want with successful file downloaded and then 
                // call the method again to get the next file
              console.log("Current Item"+Index+"Code = " + r.responseCode);
              console.log("Current Item"+Index+"DResponse = " + r.response);
              console.log("Current Item"+Index+"Sent = " + r.bytesSent);
             
            }, function(r) {
             alert("Current Item"+Index+"An error has occurred: Code = " + error.code);
            console.log("Current Item"+Index+"upload error source " + error.source);
            console.log("Current Item"+Index+"upload error target " + error.target);
            }, options);
                     
      

           


}







function DoUploadCamera (Index){
  
 
    var newIndex = (Index);
  console.log("Uploading Call For -->"+Index+"-->"+ "removed" + "   ID--> "+id+" New Index --> " + newIndex);
    var FileName="Unknown";
    switch (Index) {
            case 0:
               FileName= id+"_CompanyLogoPhoto.jpeg";
               console.log("Current File-->"+FileName + "  Index-->"+Index );
                
                break;
            case 1:
                  FileName= id+"_RELicense.jpeg";
               console.log("Current File-->"+FileName + "  Index-->"+Index );
                break;

           case 2:
                  FileName= id+"_CLPhoto.jpeg";
               console.log("Current File-->"+FileName + "  Index-->"+Index );
                break;

           case 3:
                  FileName= id+"_CIDPhoto.jpeg";
               console.log("Current File-->"+FileName + "  Index-->"+Index );
                break;
            default:
             FileName= "_undefined.jpeg";
               console.log("Current File-->"+FileName + "  Index-->"+Index );
               break;

        }
  
    var uri = window.localStorage.getItem('domain')+'/api/KwtrgDB/SaveImage';
            $ionicLoading.show({			
								duration:30000,	 
								noBackdrop: true,
                                hideOnStateChange:true,
								 
								});
        var options = new FileUploadOptions();
        options.fileKey="Filedata";
        options.fileName=remoteFiles[0].substr(remoteFiles[0].lastIndexOf('/') + 1);
          options.chunkedMode= false;
        options.mimeType= "image/*";
          var params = new Object(); 
                    params.session1 = FileName ; 
                
                    options.params = params; 
        var headers={'headerParam':'headerValue'};

        options.headers = headers;

        var ft = new FileTransfer();
        ft.onprogress = function(progress) {
           console.log("Onprog-->"+newIndex+"-->");
            var percent  = Math.round(progress.loaded / progress.total *360);
                  var loader = document.getElementById('b_'+ newIndex), 
                      border = document.getElementById('l_'+ newIndex),
                      a = 0, 
                    p = Math.PI; 
                  
                  (function draw() {
                    a = percent; 
                    a %= 360;
                    var r = a * p / 180, x = Math.sin(r) * 125, y = Math.cos(r) * -125, mid = a > 180 ? 1 : 0, 
                        anim = 'M 0 0 v -125 A 125 125 1 ' + mid + ' 1 ' + x + ' ' + y + ' z';
                  
                    loader.setAttribute('d', anim); 
                    border.setAttribute('d', anim);
                   console.log("a value -->"+a);
                  if (a !== 0){
                    draw();
                  } else if (a === 0){
                         $ionicLoading.hide({	});       
                      console.log("removing blur -->"+a);
                    $('#I_'+ newIndex).removeClass("clz_blur"); 
                  }
                    
                  }()); 
        };
        ft.upload(remoteFiles[0], uri,function(r) {
                // Do what you want with successful file downloaded and then 
                // call the method again to get the next file
                  
              console.log("Current Item"+Index+"Code = " + r.responseCode);
              console.log("Current Item"+Index+"DResponse = " + r.response);
              console.log("Current Item"+Index+"Sent = " + r.bytesSent);
               
                 
            }, function(r) {
             alert("Current Item"+Index+"An error has occurred: Code = " + error.code);
            console.log("Current Item"+Index+"upload error source " + error.source);
            console.log("Current Item"+Index+"upload error target " + error.target);
            }, options);
                     
}//END OF CAMERA UPLOAD





      }//END OF CONTROLLER    
		 
	 
	
})();