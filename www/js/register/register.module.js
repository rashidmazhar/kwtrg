(function() {
	'use strict';
	
 


	angular
		.module('kwtrg.register', [
			'ionic'
		])
		.config(function($stateProvider) {
			$stateProvider

			.state('register', {
					url: '/register',
					 
					templateUrl: 'js/register/register.html',
					controller: 'registerController as vm'
				});
			;
			
		});
})();
