(function() {
	'use strict';

	var propertiesView = {
		templateUrl: 'js/request/requests.html',
		controller: 'RequestsController as vm'
	};

	angular
		.module('kwtrg.request', [
			'ionic',
			'angucomplete-alt'
			 
		])
		.config(function($stateProvider) {
			$stateProvider
				.state('app.tabs.requests', {
					url: '/requests/:TypeId/:serviceId',
				views: { 'tab-setting': propertiesView },
				resolve: {
						filterModal: function($ionicModal, $rootScope) {
							return $ionicModal.fromTemplateUrl('js/request/item-filter.html', {
								scope: $rootScope,
								animation: 'slide-in-up'
							});
						}
					}
				 
				})
				;
		});
})();