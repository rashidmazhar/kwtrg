(function() {
	'use strict';

	angular
		.module('kwtrg.request')
		.factory('requestService', requestService);

	requestService.$inject = ['$http', '$q'];

	/* @ngInject */
	function requestService($http, $q) {
	//var url ='http://skounis.s3.amazonaws.com/mobile-apps/barebone-glossy/request.json';
		
		 
		var result = [];
        
		var service = {
			all: all,
			get: get,
			allResults: allResults,
			allCountry:allCountry,
			allCity:allCity,
			allRegion:allRegion,
			allTypes:allTypes,
			getFeature:getFeature,
			allSort:allSort,
			GetResult:GetResult,
			CallCounter:CallCounter,
			UpdateOpneToke:UpdateOpneToke
		};
		return service;

		// *******************************************************

		// http://stackoverflow.com/questions/17533888/s3-access-control-allow-origin-header
		function all(callback , TypeId, serviceID, CountyID){
			var token  =  window.localStorage.getItem('registrationId');

				if (token === undefined || token ==="" || token===null || token==="null")
				{
					token="cwbnXpBr8aI:APA91bEcJQAQRw-wLqgHS_wKqVb4ZAEJNxDejWiQ1Hqgn9tK7F-zOX5zeVpf8ZTpjnFfXNVRxoL4GgFCTdWK90TYSTW22YkRTbJ3L5hTWBFULmIPoL41fvu9XscMJXoIAqxqe4FcMWzZ";
				}
	
         var url = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetItemsRequest?_token='+token;
			$http.get(url)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					result = data;				
					callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					//alert('no');
					console.log('ERROR (News):'+data + status);
					callback(result);
				});
		}
        

	function allSort(callback , Item ){

		var token  =  window.localStorage.getItem('registrationId');
	
		if (token === undefined || token ==="" || token===null || token==="null")
		{
			token="fx7_ym7VDsY:APA91bF5z1kK0cVgcmzCk1Uf2b_HPQKAeDmN4-hgx2qNz2CWoyY5UiOmvZWQCoA6QTBBJeORpB1AyyixDCMYys70_xhpwQ0d51GDkgtmuMqKMF5oRKoNMtWnLADamSWpfhw8H6mcbD_i";
		}

		Item.token = token;
         var url = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetRequestFilter';
		// ?countryID=1&TypeId='+TypeId+'&Lang='+window.localStorage.getItem('lang')+'&serviceID='+serviceID+'&SelectedCountry='+selectedcountry+'&City_ID='+selectedCity+'&Region_ID='+selectedRegion+'&SelectedType='+selectedTypeProperty+'&sortOrder='+sortBy+'&Feature='+JSON.stringify(Feature); 
		 
			  $http({
                url: url,
                method: 'POST',
				dataType: "json",
                data:  Item,
				contentType: "application/json",
              //  headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            })
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
				 	result = data;
 
					callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					//alert('no');
					console.log('ERROR (News-alSort):'+data + status);
					callback(result);
				});
		}


		function allResults(callback , TypeId, serviceID, SearchString){
         var url = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetItemsSearchString?countryID=1&TypeId='+TypeId+'&Lang='+window.localStorage.getItem('lang')+'&serviceID='+serviceID+'&SearchString='+SearchString; 
			$http.get(url)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					//alert(result);
					result = data;
					
					callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					//alert('no');
					console.log('ERROR (News-allResults):'+data + status);
					callback(result);
				});
		}
         



        
		function allCountry(callback){
		 var urlCountry = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetCountriesApp?Lang='+window.localStorage.getItem('lang');
			$http.get(urlCountry)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					result = data;
			 
				    
					callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					//alert('Error : Try Again');
					console.log('ERROR (request-allCountry):'+data + status);
					callback(result);
				});
			
			  
			 
		}


	 function allCity(callback, id){
		
		 var urlCity = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetCitiesByCountriesApp?Lang='+window.localStorage.getItem('lang')+"&id="+id;
			$http.get(urlCity)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					result = data;
				 
					callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					//alert('Error : Try Again');
					console.log('ERROR (request-allCity):'+data + status);
					callback(result);
				});	 
		}
     

	       function allRegion(callback, id){
		 var urlCity = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetRegionsByCitiesApp?Lang='+window.localStorage.getItem('lang')+"&id="+id;
			$http.get(urlCity)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					result = data;
				    
					callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					//alert('Error : Try Again');
					console.log('ERROR (request-allRegion):'+data + status);
					callback(result);
				});
			
			  
			 
		}  
	  function allTypes(callback, id){
		 var urlType = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetTypeByCountryIDApp?Lang='+window.localStorage.getItem('lang')+"&id="+id;
			$http.get(urlType)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					result = data;
				    
					callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					//alert('Error : Try Again');
					console.log('ERROR (request-allTypes):'+data + status);
					callback(result);
				});
			
			  
			 
		} 
			

		function getFeature(callback ,TypeID,CountryID) {
		 
		 var urlFeature = window.localStorage.getItem('domain')+'/api/KwtrgDB/GetTypeFieldsApp?Lang='+window.localStorage.getItem('lang')+"&Type="+TypeID+"&CountryId="+CountryID;
		 
		$http.get(urlFeature)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					result = data;
				  
					callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					//alert('Error : Try Again');
					console.log('ERROR (request-getFeature):'+data + status);
					callback(result);
				});
	}

		function get(callback,requestId) {
			// we take an request from cache but we can request ir from the server
			var urlItem=  window.localStorage.getItem('domain')+'/api/KwtrgDB/GetItemsFull?itemID='+requestId+'&Lang='+ window.localStorage.getItem('lang');
		        $http.get(urlItem)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					result = data;
					
					
					callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					//alert('no');
					console.log('ERROR (News-get):'+data + status);
					callback(result);
				});
		}

		function GetResult (callback,term,ServiceId,TypeID,CounrtyID)
		{
			 
			var urlSearch=  window.localStorage.getItem('domain')+'/api/KwtrgDB/GetItems?ServiceId='+ServiceId+'&TypeID='+TypeID+'&countryID='+CounrtyID+'&term='+term+'&Lang='+window.localStorage.getItem('lang');
				$http.get(urlSearch)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					result = data;
				
					callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					//alert('no');
					console.log('ERROR (Search-GetResult):'+data + status);
					callback(result);
				});
		}

		function  CallCounter(callback, ItemID) {

			var urlCallCounter=  window.localStorage.getItem('domain')+'/api/KwtrgDB/CallCounter?itemID='+ItemID;
			    
				$http.get(urlCallCounter)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					result = data;
				
					//callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					//alert('no');
					console.log('ERROR (Search-CallCounter):'+data + status);
					callback(result);
				});

			
		}

		function UpdateOpneToke(callback, ItemID ){
			var token  =  window.localStorage.getItem('registrationId');
	
				if (token === undefined || token ==="" || token===null || token==="null")
				{
					token="fx7_ym7VDsY:APA91bF5z1kK0cVgcmzCk1Uf2b_HPQKAeDmN4-hgx2qNz2CWoyY5UiOmvZWQCoA6QTBBJeORpB1AyyixDCMYys70_xhpwQ0d51GDkgtmuMqKMF5oRKoNMtWnLADamSWpfhw8H6mcbD_i";
				}
			  
			 var url = window.localStorage.getItem('domain')+'/api/KwtrgDB/ReadBo3qar?_token='+token +'&_itemID='+ItemID; 
				$http.get(url)
					.success(function(data, status, headers, config) {
						// this callback will be called asynchronously
						// when the response is available
						result = data;
						//alert(data);
						callback(result);
					})
					.error(function(data, status, headers, config) {
						// called asynchronously if an error occurs
						// or server returns response with an error status.
						alert('no');
						console.log('ERROR (Count):' + status);
						callback(result);
					});
			}
	}
})();