(function() {
	'use strict';

	angular
		.module('kwtrg.request')
		.controller('RequestsController', RequestsController);

	RequestsController.$inject = ['$rootScope','$scope', '$state', 'requestService','$ionicScrollDelegate' , 'ionicMaterialMotion','$stateParams','filterModal','$filter','$ionicLoading','filterFilter','$ionicNavBarDelegate'];


	/* @ngInject */
	function RequestsController($rootScope,$scope, $state, requestService, $ionicScrollDelegate,ionicMaterialMotion,$stateParams,filterModal,$filter,$ionicLoading,filterFilter,$ionicNavBarDelegate) {
	
	
        var TypeId = parseInt($stateParams.TypeId);
		var serviceID = parseInt($stateParams.serviceId);
	    var allcountry = JSON.parse( window.localStorage.getItem('allCountry'));
		var allCity = JSON.parse( window.localStorage.getItem('allCity'));
		var allRegions = JSON.parse( window.localStorage.getItem('allRegion'));
		var allTypes = JSON.parse( window.localStorage.getItem('Gallary')); 
		var Lang=window.localStorage.getItem('lang');
	    var CountryID =parseInt(window.localStorage.getItem('Country_ID'));
        var searchText ='';
 
		//alert ("ok-->"+ allFeature);
	
		var vm = angular.extend(this, {
			requests: [],
			navigate: navigate,
			Country: [],
			sortBy: 'Date',
			selectedCategory: '1',
			doRefresh: doRefresh,
			submitSearch: submitSearch,
			nextAccountsWalkthrough:nextAccountsWalkthrough,
		    showAccountsWalkthrough:false,
			CountryID: CountryID, 
			TypeId:parseInt($stateParams.TypeId),
			serviceID:parseInt($stateParams.serviceId),
			showFilter:showFilter,
			applyFilters:applyFilters,
			changeCountry:changeCountry,
			changeService:changeService,
			City: [],
			selectedcountry: CountryID,
			selectedService:'', 
			selectedCity:'',
			selectedTypeProperty: TypeId,
			selectedRegion:'',
			changeCity:changeCity,
			changeRegion:changeRegion,
			changeFeature:changeFeature,
			changeType:changeType,
			Region: [],
			Feature:[],
			Types:[],
			somePlaceholder:$filter('translate')('Search'),

		});



		//TOUR
      
	var mainTour=window.localStorage.getItem('ItemsTour');
	 
	if (mainTour==1  || mainTour === null || !angular.isDefined(mainTour) )
	{
		setTimeout(function(){ 
					vm.showAccountsWalkthrough = true;
						window.localStorage.setItem('ItemsTour',0);
				
			}, 500);
		}
	 

		$scope.accountsWalkthrough = {
			steps : [
				{
					 
					text : "img/Searche.png"
				},
				
				{
					 
					text : "img/Searcha.png"
				}
			],
			currentIndex : 0
		};



		

function nextAccountsWalkthrough() {
		 
		console.log('yes');
			ionic.requestAnimationFrame(function() {
			console.log($scope.accountsWalkthrough.currentIndex);
				console.log($scope.accountsWalkthrough.steps.length-1);
				if($scope.accountsWalkthrough.currentIndex < $scope.accountsWalkthrough.steps.length-1) {
					$scope.accountsWalkthrough.currentIndex++;
					vm.showAccountsWalkthrough = true;
				}
			});

		}
		//ENDOFTOUR
        
			$scope.$on("$ionicView.beforeEnter", function(event, data){
			
				$ionicNavBarDelegate.showBackButton(false);

				$ionicLoading.show({
					duration:30000,	 
					noBackdrop: true,
					template: ' <div class="spinner2">  <ion-spinner  icon="ripple" class="spinner-assertive"></ion-spinner>  </div> </ion-content>'
	
					 });
		
					requestService.all(function(data){
								vm.requests = data;
								$ionicLoading.hide({	});
							}, TypeId,serviceID,CountryID); 


			});

		$scope.$on('$ionicView.loaded', function (event, viewData) {
         viewData.enableBack = true;
		  getCountry();
  	    

				}); 

         

		 


		function navigate(requestId) {
			$state.go('app.request', { requestId: requestId,IsBo3qar:'0' });
		}

		function doRefresh() {
         $scope.$broadcast('angucomplete-alt:clearInput');
			 vm. somePlaceholder=$filter('translate')('Search')
		setTimeout(
				requestService.all(function(data){
				vm.requests = data;
				$scope.$broadcast('scroll.refreshComplete');
				 vm.selectedCity='';
				 vm.selectedRegion='';
			}, TypeId,serviceID,CountryID)

		, 30000)
		
		}
  


			function submitSearch() {
				 

			   cordova.plugins.Keyboard.close();
                //cordova.plugins.Keyboard.close();
  	         	$ionicLoading.show({
			    duration:30000,	 
				noBackdrop: true,
				template: ' <div class="spinner2">  <ion-spinner  icon="ripple" class="spinner-assertive"></ion-spinner>  </div> </ion-content>'

             	});
                 
				requestService.GetResult(function(data){
							vm.requests = data;
							$ionicLoading.hide({	});
						},searchText, serviceID,TypeId,CountryID); 

				 $ionicScrollDelegate.scrollTop();
				 
				}


		 
		function showFilter() {
 
					var scope = filterModal.scope;
					scope.vm = {
						Country: vm.Country, 
						City:vm.City, 
						Region:vm.Region,
						Types:vm.Types,
						Feature:vm.Feature,
						selectedcountry: vm.selectedcountry,
						selectedService:vm.selectedService,
						selectedCity:vm.selectedCity,
						selectedRegion:vm.selectedRegion,
				        selectedTypeProperty: vm.selectedTypeProperty,
						sortBy: vm.sortBy,
						applyFilters: applyFilters,
						changeCountry:changeCountry,
						changeService:changeService,
						changeCity:changeCity,
						changeRegion:changeRegion,
						changeType:changeType,
						changeFeature:changeFeature
					};

					filterModal.show();
				}


			 

				

          function applyFilters() {
			filterModal.hide();
              
			var scope = filterModal.scope;
			//alert(vm.sortBy +"Country"+scope.vm.selectedcountry+"City"+ vm.selectedCity +"Region"+ vm.selectedRegion +"Type"+ scope.vm.selectedTypeProperty +"Feature"+ vm.Feature );
			vm.selectedCategory = scope.vm.selectedcountry;
			vm.sortBy = scope.vm.sortBy;
			getProperties();
				 $ionicScrollDelegate.scrollTop();
				 
		}

         
		function getProperties() {
		
    
				var scope = filterModal.scope;

				   var Item = 
                     {
					   "countryID":CountryID,
                       "TypeId":vm.TypeId,
					   "Lang:":Lang,
                        "serviceID":serviceID,
						"selectedcountry":scope.vm.selectedcountry,
						"selectedService":scope.vm.selectedService,
						"selectedCity":scope.vm.selectedCity,
						"selectedRegion":scope.vm.selectedRegion,
						"selectedTypeProperty":scope.vm.selectedTypeProperty,
						"Feature":scope.vm.Feature,
						"sortBy":scope.vm.sortBy,
						"token":"0"
                     };
					//  alert ( "countryID"+Item[ "countryID"]+ "TypeId"+ Item[ "TypeId"]+"serviceID"+ Item["serviceID"]+ "selectedcountry"+ Item["selectedcountry"]+ "selectedCity"+ Item["selectedCity"]+"selectedRegion"+Item["selectedRegion"] + "selectedTypeProperty" +Item["selectedTypeProperty"]+"Feature"+ Item["Feature"] +"sortBy" + Item["sortBy"] );
			 $ionicLoading.show({
			    duration:30000,	 
				noBackdrop: true,
				template: ' <div class="spinner2">  <ion-spinner  icon="ripple" class="spinner-assertive"></ion-spinner>  </div> </ion-content>'

             	});
			requestService.allSort(function(data){
			vm.requests = data;
			 $ionicLoading.hide({	});
		}, Item);
		}

		function getCountry() {
		   	 vm.Country=  allcountry;
			 getCity(vm.selectedcountry);
			  vm.Types=  '';  
			//  vm.Types=  filterFilter(allTypes, {Country_ID:parseInt(vm.selectedcountry)},true);
			//vm.Types= '';   
             getType(TypeId);	 
		}


      function changeCountry(selectedItem) {
		  vm.selectedcountry= selectedItem;
		  vm.selectedService='',
		  vm. selectedCity='';
		  vm.selectedRegion='';
		  vm.selectedTypeProperty='';
		  vm.Feature='';
		  vm.TypeId ='';
	      vm.City =	filterFilter(allCity, {Country_ID:parseInt(selectedItem)},true);
		  vm.Types= '';   
		  showFilter(); 
		 filterModal.show();
		}


		function    changeService  (selectedItem, SelectService) {

			//  alert("yes"+ "-->" + selectedItem + "-->" + SelectService );
			vm.selectedService=SelectService;
		 
			vm.Types=  filterFilter(allTypes, {Country_ID:parseInt(selectedItem), Service_ID:parseInt(SelectService) },true);   
		    showFilter(); 
			filterModal.show();
		
  
		 };		


      function changeCity(selectedItem) {
		    vm.selectedCity=selectedItem;
			vm.selectedRegion='';
			
			vm.Region =	filterFilter(allRegions, {City_ID:parseInt(selectedItem)},true);
		    showFilter();  
	        filterModal.show();
		}

		function changeRegion (selectedItem)
		{
			vm.selectedRegion= selectedItem;
		}
		
		 function getCity(selectedItem) {

			vm.City =	filterFilter(allCity, {Country_ID:parseInt(selectedItem)},true);
		 
		}




       function changeType (selectedType) {
               
              vm.selectedTypeProperty=parseInt(selectedType);
              requestService.getFeature(function(data){
		           	vm.Feature = data;             
                  showFilter();
                },selectedType,vm.selectedcountry);  

				
                // vm.Feature =	filterFilter(allFeature, {Country_ID:parseInt(vm.CountryID), Type:parseInt(selectedType)  },true);
			    //  showFilter();
				 filterModal.show();
             };  


		    function getType (selectedType) {
               
               
              requestService.getFeature(function(data){
		           	vm.Feature = data;             
                 
                },selectedType,vm.CountryID);  
             };  	 



       function changeFeature (selectedFeature) {
		  // alert(selectedFeature);
                var array = selectedFeature.split(';');
                var Ucs_Id = parseInt(array[0].trim());
                var Ucs_Value = parseInt(array[1].trim()) ;   
                $filter('filter')(vm.Feature, function (d) {return d.Ucs_Id === Ucs_Id;})[0]["SelectedValue"]=Ucs_Value;      
            
              
             };  

		 $scope.selectedCountry= function($item) {$state.go('app.request', { requestId: $item.originalObject.value });	 };
          $scope.inputChanged= function($item) {searchText= $item ;	 };
		 
		  
		 
	}
})();