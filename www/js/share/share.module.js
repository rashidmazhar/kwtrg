(function() {
	'use strict';
	
 


	angular
		.module('kwtrg.share', [
			'ionic'
		])
		.config(function($stateProvider) {
			$stateProvider

			.state('app.share', {
					url: '/share',
					 
				views: {
						'menuContent': {
							templateUrl: 'js/share/share.html',
						 controller: 'shareController as vm'
						}
					}
				});
			 
			;
			
		});
})();
