(function() {
	'use strict';

	angular
		.module('kwtrg.login')
		.factory('loginService', loginService);

	loginService.$inject = ['$http', '$q'];
	
	 

	/* @ngInject */
	function loginService($http, $q) {
		
		var result = [];

		var service = {
			loginUser: loginUser,
			getToken: getToken
		 
		};
		return service;

		// *******************************************************
 
		function loginUser(callback,username,password){
			 
		var url = window.localStorage.getItem('domain')+'/api/KwtrgDB/Login?UserName='+username+'&PasswordToLogin='+password;
		 
			$http.get(url)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					result = data;
				 
					callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
				//	alert('no');
					console.log('ERROR (login):' +data+ status);
					callback(result);
				});
			
			  
			 
		}

		function getToken(data){
			var	UserID ="";
			if (data !==null)
			{
			 
				 UserID = data.User_ID;
				 console.log ("We Have user " + UserID["User_ID"]);
			}
			 
			var token  =  window.localStorage.getItem('registrationId');
			var ios=0;
			if(ionic.Platform.platform() === 'ios') {
				console.log(' notification Set Value for register  IOS  ' );
				ios=1;
			  }
			 
			var url =  window.localStorage.getItem('domain')+'/api/KwtrgDB/GetToken?_userId='+UserID+"&_token="+token+"&_ios="+ios;
			 console.log(url);
				$http.get(url)
					.success(function(data, status, headers, config) {
						// this callback will be called asynchronously
						// when the response is available
						 
						if (data==="true")
						{
						 console.log('registrationId', token);
						  localStorage.setItem('registrationId', token);
						}
						else
						{
						  console.log('No registrationId', token);
						  delete localStorage['registrationId'];
						}
					})
					.error(function(data, status, headers, config) {
						// called asynchronously if an error occurs
						// or server returns response with an error status.
					//	alert('no');
					delete localStorage['registrationId'];
						console.log('ERROR (Token Registeration ):' +data+ status);
						callback(result);
					});
				
				  
				 
			}
		
		

		 
	}
})();