(function() {
	'use strict';
	
	angular
		.module('kwtrg.slider', [
			'ionic'
		])
		.config(function($stateProvider) {
			$stateProvider
			.state('app.slider', {
					url: '/slider',
					views: {
						'menuContent': {
							templateUrl: 'js/slider/slider.html',
						 controller: 'sliderController as vm'
						}
					}
				}); 
				
		});
})();
