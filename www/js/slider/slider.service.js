(function() {
	'use strict';

	angular
		.module('kwtrg.slider')
		.factory('sliderService', sliderService);

	sliderService.$inject = ['$http', '$q'];
	
	 

	/* @ngInject */
	function sliderService($http, $q) {
		var url = window.localStorage.getItem('domain')+'/api/KwtrgDB/getsliders?Lang='+window.localStorage.getItem('lang');
		var result = [];

		var service = {
			Getslider: Getslider,
		 
		};
		return service;

		// *******************************************************
 
		function Getslider(callback){
		 
			$http.get(url)
				.success(function(data, status, headers, config) {
					// this callback will be called asynchronously
					// when the response is available
					result = data;
				 
					callback(result);
				})
				.error(function(data, status, headers, config) {
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					alert('no');
					console.log('ERROR (slider):' + status);
					callback(result);
				});
			
			  
			 
		}
		
		

		 
	}
})();