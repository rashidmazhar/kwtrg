(function() {
  "use strict";

  angular.module("kwtrg.tabs").controller("TabsController", TabsController);

  TabsController.$inject = [
    "$scope",
    "$state",
    "mainService",
    "$ionicModal",
    "$interval",
    "$ionicLoading"
  ];

  /* @ngInject */
  function TabsController(
    $scope,
    $state,
    mainService,
    $ionicModal,
    $interval,
    $ionicLoading
  ) {
    var vm = angular.extend(this, {
      IsRegister: 0,
      navigate2: navigate2,
      navigate3: navigate3
    });

    var firtCall = true;
    var currentCountry = JSON.parse(window.localStorage.getItem("Country_ID"));
    var skipAd = 0;
    var userInfo = -1;
    var IsRegister = 0;
    userInfo = JSON.parse(window.localStorage.getItem("user"));

    $scope.$on("$ionicView.enter", function(event, viewData) {
      if (window.localStorage.getItem("user") === null) {
        vm.IsRegister = 0;
      } else {
        vm.IsRegister = 1;
      }
      // POPUP
      if (
        firtCall ||
        currentCountry !== JSON.parse(window.localStorage.getItem("Country_ID"))
      )
        setTimeout(function() {
          //console.log(currentCountry);
          currentCountry = JSON.parse(
            window.localStorage.getItem("Country_ID")
          );
          //console.log(currentCountry);
          firtCall = false;
          // POPUP AD
          mainService.getPopupAd(function(data) {
            data.result[0].valid_ad = true;
            var skip_text = "Waiting...";
            var skip_counter = 1;
            var skip_time = 0;
            if (data != null && data.status === "OK") {
              skip_time = data.result[0].skip_time;

              vm.popupad = "data:image/png;base64," + data.result[0].image;
              vm.skip_count = skip_text;
              vm.adlink = data.result[0].link;

              $ionicLoading.show({
                duration: 3000,
                noBackdrop: true,
                template:
                  ' <div class="spinner2">  <ion-spinner  icon="ripple" class="spinner-assertive"></ion-spinner>  </div> </ion-content>'
              });

              //window.localStorage.getItem('showAd') === null &&
              if (data.result[0].valid_ad === true) {
                $scope.modal.show();
                //window.localStorage.setItem("showAd", 1);
              }

              var timer = $interval(function() {
                var skip_diff = skip_time - skip_counter;
                vm.skip_count = skip_diff;
                if (skip_counter === skip_time) {
                  vm.can_skip = true;
                  vm.skip_count = "Skip Ad";
                  skipAd = 1;
                  $interval.cancel(timer);
                }

                //console.log(skip_counter);
                skip_counter++;
              }, 1000);

              //console.log("/********************AD*************************/");
            }
          });
        }, 0);
      //END POP UP
      $ionicModal.fromTemplateUrl(
        "modal.html",
        function($ionicModal) {
          $scope.modal = $ionicModal;
        },
        {
          // Use our scope for the scope of the modal to keep it simple
          scope: $scope,
          // The animation we want to use for the modal entrance
          animation: "slide-in-up"
        }
      );

      $scope.openModal = function() {
        $scope.modal.show();
      };

      $scope.close_add = function() {
        if (skipAd === 1) $scope.modal.hide();
      };

      $scope.open_link = function(link) {
        //console.log(link);
        if (link !== null) window.open(link, "_system", "location=yes");
      };
    });

    function navigate2() {
      if (window.localStorage.getItem("user") === null) {
        $state.go("app.tabs.AddBo3qar");
      } else {
        $state.go("app.tabs.AddItem");
      }
    }

    function navigate3() {
      if (window.localStorage.getItem("user") === null) {
        $state.go("app.tabs.bo3qars");
      } else {
        $state.go("app.tabs.myAds");
      }
    }
  }
})();
