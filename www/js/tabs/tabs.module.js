(function() {
	'use strict';

	angular
		.module('kwtrg.tabs', [
			'ionic'
		])
		.config(function($stateProvider) {
			$stateProvider
				  	.state('app.tabs', {
					url: '/tabs',
					abstract: true,
					views: {
						'menuContent': {
							templateUrl: 'js/tabs/tabs.html',
							controller: 'TabsController as vm',
						}
					}
				
				})
		});
})();