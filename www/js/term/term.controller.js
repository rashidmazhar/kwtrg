(function() {
	'use strict';

	angular
		.module('kwtrg.term')
		.controller('termController', termController);

	termController.$inject = [
		 '$stateParams',
	 
		
		'$timeout',
		 
		 
		'termService',
		 '$scope',
		 '$ionicLoading',
	];

	/* @ngInject */
	function termController($stateParams,
		 $timeout, termService,$scope,$ionicLoading) {
		var vm = angular.extend(this, {
			entries: '',		 
		});
      
       
		
		(function activate() {
              	$ionicLoading.show({			
								duration:30000,	 
								noBackdrop: true,
								template: ' <div class="spinner2">  <ion-spinner  icon="ripple" class="spinner-assertive"></ion-spinner>  </div> </ion-content>'
								});
			   termService.GetTerm(function(data){
		           	$scope.snippet = data;    
					   $ionicLoading.hide({	});         
                 
                });  
		       
		})();

		// **********************************************************	 
	}
})();