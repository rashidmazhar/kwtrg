(function() {
	'use strict';
	
	angular
		.module('kwtrg.term', [
			'ionic'
		])
		.config(function($stateProvider) {
			$stateProvider
			.state('app.term', {
					url: '/term',
					views: {
						'menuContent': {
							templateUrl: 'js/term/term.html',
						 controller: 'termController as vm'
						}
					}
				}); 
				
		});
})();
